exports.queryExecutor = function(index, queryString, req, res, esClient){
	var totalHits;
	esClient.search({
		  index: 'knowledgebase',
		  // type: '_doc', // uncomment this line if you are using Elasticsearch ≤ 6
		  body: {
		    query: {
				        multi_match: {
			            query: queryString,
			            type: "most_fields",
			            fields:['answer','question', 'title']
			        }
		    }
		  },
		  size: 15
		}, function (err, data) {
			totalHits = data.hits.hits;
//			console.log('total hits are ' + data.hits.hits.total);
		   if (err) return console.error(err);
		    console.log(`found ${data.hits.total} items in ${data.took}ms`);
//		    console.log('size of result list is ###' + data.hits.hits.length);
		    var newHitsArray = [];
		    data.hits.hits.forEach(
		   	     (hit, index) => {
//		   	    	console.log(
//		 		   	       `${hit._source.question}`
//		 		   	     )
		 		   	var forumLogos = ['alexia', 'avocat-omer', 'droitsquotidiens', 'experatoo', 'legavox', 'net-iris', 'phpbb'];
		   	    	for(i=0;i<forumLogos.length;i++){
//		   	    		console.log('pageURL is ' + `${hit._source.pageURL}`);
			 		   	if(`${hit._source.pageURL}`.indexOf(forumLogos[i])!==-1){
//			 		   		console.log(forumLogos[i] + ' was found');
			 		   		data.hits.hits[index].image = "assets/img/alexia.png";
//			 		   		console.log('the index in question is ' + index);
//			 		   		console.log("$$ image value is " + data.hits.hits[index].image);
			 		   		newHitsArray[index] = new Object();
			 		   		newHitsArray[index].image = "assets/img/" + forumLogos[i] + ".png";
			 		   		newHitsArray[index].title = `${hit._source.title}`;
			 		   		newHitsArray[index].answer = `${hit._source.answer}`;
			 		   		newHitsArray[index].pageURL = `${hit._source.pageURL}`;		 		   		
			 		   	}
		   	    	}
     
		   	     }
		   	   )		    
		    // Render results in a template.
		    res.render('searchresult', {
		      hits: newHitsArray,
		      total: data.hits.total,
		      queryString: queryString
		    });			    

//	   	res.render('searchresult', {hits: data.hits.total});
		})
//		.then((resp) => {
//			var resultList = [];
//			var record = {};
////			resp.hits.hits.forEach(
////					(hit, index) => {record = {
////							'question' : `${hit._source.question}`,
////							'answer' : `${hit._source.answer}`,
////					}
////					resultList.push(record);
////					)
//			
//		    // Render results in a template.
//		    res.render('searchresult', {
//		      hits: resp.hits.hits,
//		      total: resp.hits.total,
//		      resultList: resultList
//		    });			
//		});	
}