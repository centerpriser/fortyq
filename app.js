const https = require('https');
var http = require('http');
var express = require('express');
var fs = require('fs');
const expressValidator = require('express-validator');
var app = express();
var bodyParser = require('body-parser');
const hbs = require('express-hbs');
var session = require('express-session');
var cookieParser = require('cookie-parser');
const db = require('./postgres/index');
var cors = require('cors');

//var redirectToHTTPS = require('express-http-to-https').redirectToHTTPS;
//credentials for https
var key = fs.readFileSync(__dirname + '/certs/private.key');
var cert = fs.readFileSync(__dirname + '/certs/certificate.crt');
var options = {
  key: key,
  cert: cert
};

//session store information
const redis = require('redis')

var RedisStore = require('connect-redis')(session);
var redisClient = redis.createClient();

//increase size of payload
app.use(bodyParser.json({ limit: '10mb' }));
app.use(bodyParser.urlencoded({ extended: true, limit: '10mb' }));
app.use(cors());

//The i18n module is loaded
var i18n = require('i18n');
i18n.configure({
	  locales: ['en', 'fr'],
	  cookie: 'locale',
	  directory: __dirname + "/locales",
	  defaultLocale: 'en',
	 // query parameter to switch locale (ie. /home?lang=ch) - defaults to NULL
      queryParameter: 'lang'		  
	});


//use sessions for tracking logins
app.use(session({
  secret: 'work hard',
  resave: true,
  saveUninitialized: false,
  store: new RedisStore({ client: redisClient })
}));


app.use(expressValidator());

////handle mongo error
//db.on('error', console.error.bind(console, 'connection error:'));
//db.once('open', function () {
//// we're connected!
//});


//Use `.hbs` for extensions and find partials in `views/partials`.
app.engine('hbs', hbs.express4({
  partialsDir: __dirname + '/views/partials',
  i18n: i18n
}));

	
app.use(express.static('views'));
app.set('view engine', 'hbs');
app.set('views', __dirname + '/views');

//increase size of payload
app.use(bodyParser.json({ limit: '10mb' }));
app.use(bodyParser.urlencoded({ extended: true, limit: '10mb' }));

// you'll need cookies
app.use(cookieParser());
app.use(i18n.init);

//set language
//app.get('/:locale', function (req, res) {
//	  res.cookie('locale', req.params.locale);
//	  res.redirect(req.headers.referer);
//	});

//include routes
var routes = require('./routes/router');
app.use('/', routes);

//session creation
app.use(session({secret: 'ssshhhhh',saveUninitialized: true,resave: true}));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('File Not Found');
  err.status = 404;
  next(err);
});

// error handler
// define as the last app.use callback
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.send(err.message);
});


hbs.registerHelper('__', function () {
	   return i18n.__.apply(this, arguments);
	});

////include routes
//var routes = require('./routes/router');
//app.use('/', routes);

// listen on port 80
//app.listen(8080, function () {
//  console.log('Express app listening on port 80');
//});
//app.use(redirectToHTTPS([/localhost:(\d{4})/], 301));
app.all('*', ensureSecure); // at top of routing calls
http.createServer(app).listen(8080);

var server = https.createServer(options, app);

function ensureSecure(req, res, next){
	  if(req.secure){
	    // OK, continue
	    return next();
	  };
	  // handle port numbers if you need non defaults
	  // res.redirect('https://' + req.host + req.url); // express 3.x
	  res.redirect('https://' + req.hostname + req.url); // express 4.x
	}

server.listen(8443, () =>{
	
	console.log('server has started');
})

//http.createServer(function (req, res) {
//  res.writeHead(200, {'Content-Type': 'text/plain'});
//  res.end('How you doin?\n');
//}).listen(3000, '127.0.0.1');
//console.log('Server running at http://127.0.0.1:3000/');


//callback API
//esClient.search({
//  index: 'knowledgebase',
//  body: { answer: 'bonjour' }
//}, (err, result) => {
//  if (err) console.log(err)
//})

//const search = function search(index, body) {
//  return esClient.search({index: index, body: body});
//};
//
//const test = function test() {
//  let body = {
//    size: 20,
//    from: 0,
//    query: {
////      match_all: {}
//        match: {
//            answer: {
//              query: 'bonjour je suis ravie'
//            }
//          }    	
//    }
//  };
//
//  search('knowledgebase', body)
//  .then(results => {
//    console.log(`found ${results.hits.total} items in ${results.took}ms`);
//    console.log(`returned article titles:`);
//    results.hits.hits.forEach(
//      (hit, index) => console.log(
//        `\t${body.from + ++index} - ${hit._source.title}`
//      )
//    )
//  })
//  .catch(console.error);
//};
//test();
// Let's search!


//Comment out this code
//const search = esClient.search({
//	  index: 'knowledgebase',
//	  // type: '_doc', // uncomment this line if you are using Elasticsearch ≤ 6
//	  body: {
//	    query: {
//			        match: {
//		          answer: {
//		            query: 'bonjour je suis ravie'
//		          }
//		        }
//	    }
//	  }
//	}, function (err, data) {
//	   if (err) return console.error(err);
//	    console.log(`found ${data.hits.total} items in ${data.took}ms`);
//	    data.hits.hits.forEach(
//     (hit, index) => console.log(
//       `${hit._source.title}`
//     )
//   )
//	});


//const body = esClient.search({
//  index: 'knowledgebase',
//  // type: '_doc', // uncomment this line if you are using Elasticsearch ≤ 6
//  body: {
//    query: {
//      match: { query: 'bonjour' }
//    }
//  }
//});
//console.log('body is ' + body)