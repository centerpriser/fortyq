# -*- coding: utf-8 -*-
import scrapy
import html2text
import re
import json

# with open('/Users/ankujarora1/Documents/workspace2/fortyq/scraping/sus/0.json') as jsonFile:
#     data = json.load(jsonFile)
#     for i in data['companies']:
#         print('profile_path ', i['profile_path'])
#         print('name', i['name'])
#         print('founders', i['founders'][0]['name'])
class QuotesSpider(scrapy.Spider):
    name = "sus"
    def start_requests(self):
        with open('/Users/ankujarora1/Documents/workspace2/fortyq/scraping/sus/input.json') as jsonFile:
            data = json.load(jsonFile)
            for i in data['companies']:
#                 print('profile_path ', i['profile_path'])
#                 print('name', i['name'])
#                 print('founders', i['founders'][0]['name'])        
                yield scrapy.Request('https://www.startupschool.org/' + i['profile_path'], callback = self.parse, method="GET")
# #         yield scrapy.Request('https://www.startupschool.org/directory?page=0&same_country=true&seeking_cofounder&shared_group_session&verticals&within_50_km',
# #                                         callback=self.parse, method='GET', headers = {'authority' :'www.startupschool.org',
# #                                                                                 'path' : '/directory?page=0&same_country=true&seeking_cofounder&shared_group_session&verticals&within_50_km',
# #                                                                                 'scheme' : 'https',
# #                                                                                 'accept' : 'application/json',
# #                                                                                 'accept-encoding' : 'en-GB,en;q=0.9,en-US;q=0.8,fr;q=0.7,es;q=0.6,de;q=0.5',
# #                                                                                 'cache-control' : 'no-cache',
# #                                                                                 'referer' : 'https://www.startupschool.org/',
# #                                                                                 'sec-fetch-mode' : 'cors',
# #                                                                                 'sec-fetch-site' : 'same-origin',
# #                                                                                 'user-agent' : 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36',
# #                                                                                 'dnt' : '1',
# #                                                                                 'origin' : 'https://www.startupschool.org',
# #                                                                                 'pragma' : 'no-cache',
# #                                                                                 'accept-language' : 'en-GB,en;q=0.9,en-US;q=0.8,fr;q=0.7,es;q=0.6,de;q=0.5',
# #                                                                                 'content-type' : 'application/json',
# #                                                                                 'cookie': '__cfduid=d1f228c2127ed15349b9bf80d390166b51579018982; _ALGOLIA=anonymous-500e9a64-b08c-487a-9f60-e4b1d1393c53; _ga=GA1.2.1032141141.1579018984; _gid=GA1.2.1395661504.1579018984; _sso.key=S5G0ABjx72bRD8a8MRJeAywOsePATAyr; _gat=1; _sus_session=eTFLenNFL0VyUWkyZ2JKRHBuYUVTaDk0R0lTbFdwNEI0ckVpbGxYNnBYRkJ2dnc0ZERuTUk0WDE2YjA0eHBuQkxJbEduaWpYQ0ZpSlVBb3pDSGQ2ZkxMalI0U2ZFZ0RrWUpINFRKc2pDeG8zRHBvNDBoZXk0eDU5c0FZVkFjbUFiWXhaN0Rha2IvOFFXMGRkZks5czNnPT0tLTMxdTlpYkpDOXBJc1BuejR5S3Y4UUE9PQ%3D%3D--5f4b4ac8bd19b2e26463e8806525b5355602daca',
# #                                                                                 'x-csrf-token' : 'xG4ShhBCYWRQvw5eGmm8YCkdbEeq83lAl7GQ6EEBTjWAr50bAxAzW4KSpVyN+2ihrA+HxJUBErAouQv1guY8Hw=='
# #                                                                                 })
# 
#

     
    def parse(self, response):
        def cfDecodeEmail(encodedString):
            r = int(encodedString[:2],16)
            email = ''.join([chr(int(encodedString[i:i+2], 16) ^ r) for i in range(2, len(encodedString), 2)])
            return email 
        meta = response.css('div.top-container div.wrapper div.section.company.dark ')
        companyNameA = meta.css('div.company-header div.company-name a::text').get()
        companyNameDiv = meta.css('div.company-header div.company-name::text').get()
        email = meta.css('div.founders div.founder-name a::attr(href)').getall()
        emailList = []
        for i in email:
            j = cfDecodeEmail(i[28:].encode('utf-8'))
            emailList.append(j)
#         print('companyNameDiv ', companyNameDiv)
        if (companyNameA == None): companyName = companyNameDiv
        else: companyName = companyNameA
        if (emailList):
            founderFirstNames = [] 
            founderNames = meta.css('div.founders div.founder-name a::text').getall();
            for j in range(len(founderNames)):founderFirstNames.append(founderNames[j].split()[0])
            yield{
                  'companyName' : companyName,
                'founderNames' : founderFirstNames,
                'email' : emailList
                  }    
#         for i in response.css('div.top-container div.wrapper div.section.company.dark div.founders div.founder div.founder-name a::attr(href)'):
#             encodedMail = i.get()[28:].encode('utf-8')
#             print('decoded mail is', cfDecodeEmail(encodedMail))
            
            
#             print('email is ', i.get()[28:].encode('utf-8'))
#             print('email is ', self.cfDecodeEmail(i.getall()[0][28:]))
            
            
            
           
# #         request_with_cookies = scrapy.Request('https://api-js.mixpanel.com/decide/?verbose=1&version=1&lib=web&token=2c83bd4073c46306405f8843951b4361&ip=1&_=1579021171418',
# #                                         callback=self.after_login, method='GET', headers = {'authority' :'api-js.mixpanel.com',
# #                                                                                 'path' : '/decide/?verbose=1&version=1&lib=web&token=2c83bd4073c46306405f8843951b4361&ip=1&_=1579036545624',
# #                                                                                 'scheme' : 'https',
# #                                                                                 'accept' : '*/*',
# #                                                                                 'accept-encoding' : 'en-GB,en;q=0.9,en-US;q=0.8,fr;q=0.7,es;q=0.6,de;q=0.5',
# #                                                                                 'cache-control' : 'no-cache',
# #                                                                                 'referer' : 'https://www.startupschool.org/',
# #                                                                                 'sec-fetch-mode' : 'cors',
# #                                                                                 'sec-fetch-site' : 'cross-site',
# #                                                                                 'user-agent' : 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36'
# #                                                                                 })
# #         return scrapy.FormRequest.from_response(
# #             response,
# #             formdata={'username': 'judlaw', 'password': 'grenoblefrance'},
# #             callback=self.after_login
# #          )
#         #crawl a question and answer in the event that there are more than 1 responses to it
# #         for i in response.css('div.top-container div.signinwrapper div.signin-section div.offset-button a.orange.button.uppercase::attr(href)'):
# #             yield response.follow(i, callback = self.before_login)            
# #             numberOfResponses = i.css('ul.threadstats.td.alt li::text').getall()
# #             numberOfResponses = re.sub(r'(\r*\n*\t*)', '', numberOfResponses[0])
# #             match = re.search(':(\d*)', numberOfResponses)
# # #             print('numberOfResponses is ', match.group(1))
# #             if(int(match.group(1))>0):
# #                 yield response.follow(href[0], callback = self.parse_individualQuestion)
# 
#     def before_login(self, response):
#         return scrapy.FormRequest.from_response(
#             response,
#             formdata={'username': 'judlaw', 'password': 'grenoblefrance'},
#             callback=self.after_login
#          )
#     
#     def after_login(self, response):
#         if "Error while logging in" in response.body:
#             self.logger.error("Login failed!")
#         else:
#             print("response is" , response)
#             for i in response.css('div.full-size-menu div.menu-items a.item-container.false::attr(href)'):
#                 print('i is ' , i)
#             scrapy.Request('https://www.startupschool.org/directory?same_country=true&seeking_cofounder&shared_group_session&verticals&within_50_km',
#                                         callback=self.parse_directory, method='GET', headers = {'authority' :'www.startupschool.org',
#                                                                                 'path' : '/directory?page=0&same_country=true&seeking_cofounder&shared_group_session&verticals&within_50_km',
#                                                                                 'scheme' : 'https',
#                                                                                 'accept' : 'application/json',
#                                                                                 'accept-encoding' : 'gzip, deflate, br',
#                                                                                 'accept-language' : 'en-GB,en;q=0.9,en-US;q=0.8,fr;q=0.7,es;q=0.6,de;q=0.5',
#                                                                                 'cache-control' : 'no-cache',
#                                                                                 'content-type' : 'application/json',
#                                                                                 'cookie' : '__cfduid=d1f228c2127ed15349b9bf80d390166b51579018982; _ALGOLIA=anonymous-500e9a64-b08c-487a-9f60-e4b1d1393c53; _ga=GA1.2.1032141141.1579018984; _gid=GA1.2.1395661504.1579018984; _sso.key=GqtGzjx2YMclqL0Y6z8lq-QPOZZvLXZR; _sus_session=Qk1sSkN5N01UaExCTkROWjVwUTRheEUrRzRubm96YkZvNVJDbkRMT0YvaStNRjJLSjBTL0lyQUZWckE2VmhwVGN3eFVPQnVMc0Z1elYrNDFkUFFYMmZzODFQM2laL1lzWGpHZndpNUhTbldyZHdrc0FYS1doNWlzbFVGbWhPSDlRTHgzdU5kRTg2R1FHTkNOQXJKYjNBPT0tLVpUYnRKRU55ck9lUGhRREQveDRxU2c9PQ%3D%3D--a61680223f0d9b23e2e35a402ef615ae694d387e',
#                                                                                 'dnt' : '1',
#                                                                                 'pragma' : 'no-cache',
#                                                                                 'referer' : 'https://www.startupschool.org/directory?same_country=true&seeking_cofounder&shared_group_session&verticals&within_50_km',
#                                                                                 'sec-fetch-mode' : 'cors',
#                                                                                 'sec-fetch-site' : 'same-origin',
#                                                                                 'user-agent' : 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36',
#                                                                                 'x-csrf-token' : 'JmtvB0aHjOfVNSVRIlyHjwnVlMKMud9JhvlkGvF1XNoE+oha+b1oE2NW51hrp3VCiHDkN7HNsG+P3tnjJF7R0w=='
#                                                                                 })
#             
#             
#     def parse_directory(self, response):
#         
#         print('response.text' + response.text)
#         converter = html2text.HTML2Text()
#         converter.ignore_links = True
#         question = response.css('div.postlist ol.posts li.postbitlegacy.postbitim.postcontainer.old div.postdetails div.postbody div.postrow')[0]
# #         for href in postList:
# #         print('question is ', question.css('div.content.hasad div blockquote.postcontent.restore::text').getall())
#         answer = response.css('div.postlist ol.posts li.postbitlegacy.postbitim.postcontainer.old div.postdetails div.postbody div.postrow')[1]
# #         print('answer is ', answer.css('div.content div blockquote.postcontent.restore::text').getall())
#         dateTime = response.css('div.postlist ol.posts li.postbitlegacy.postbitim.postcontainer.old div.posthead span.postdate.old span.date::text')[1].getall()
# #         print('dateTime is', dateTime)
#         title = response.css('div.pagetitle h1 span.threadtitle a::text').getall()
#         yield{
#               'pageURL' : response.request.url,
#               'dateTime' : dateTime,
#               'title'    : title,
#               'question' : question.css('div.content.hasad div blockquote.postcontent.restore::text').getall(),
#               'answer' : answer.css('div.content div blockquote.postcontent.restore::text').getall()
#               }
