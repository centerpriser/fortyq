/*jshint esversion: 6 */
var express = require('express');
var path = require("path");
var router = express.Router();
//var elkSearch = require('../searches/elksearch.js');
//var tts = require('../tts/recognize.js');
var QAMaster = require('../models/qamaster');
var LawyerProfileMaster = require('../models/lawyerProfileMaster');
var slugify = require('slugify');
var moment = require('moment');
var crypto = require('crypto');
var nodemailer = require('nodemailer');
var i18n = require('i18n');
var bcrypt = require('bcrypt');
var formidable = require('formidable');

var transporter = nodemailer.createTransport({
	 service: 'zoho',
	 auth: {
	        user: 'contact@helplicit.com',
	        pass: 'grenoblefrance'
	    }
	});
var fs = require('fs');
const multer = require("multer");
const upload = multer({
	  dest: "uploads"
	  // you might also want to set some limits: https://github.com/expressjs/multer#limits
	});
var ssn;
//const elasticsearch = require('elasticsearch');
//const esClient = new elasticsearch.Client({
////	keep these credentials for production code
////	  host: 'http://elastic:changeme@elasticsearch:9200',
////	  keep these credentials for local code
//	  host: 'localhost:9200',	  
//	  log: 'error'
//	});	

var redis = require("redis");
var client = redis.createClient();

const config = require('../config/config.json');
const defaultConfig = config.development;
const Pool = require('pg').Pool
const pool = new Pool({
  user: defaultConfig.dbUser,
  host: defaultConfig.host,
  database: defaultConfig.database,
  password: defaultConfig.dbPassword,
  port: 5432,
})
//GET route for reading data, loading the homepage with a list of all GLOs filed
router.get('/', function (req, res) {
	ssn = req.session;
//	pool.query('SELECT * FROM users ORDER BY id ASC', (error, results) => {
//		    if (error) {
//		      throw error
//		    }
//		    else
//		    	console.log('######')
//	});
	res.render('index');
});

////POST route for user signup
//router.post('/signup', function (req, res, next) {
//	console.log('req.session '+req.session);
//	const errors = req.validationResult;
//	if (req.body.email && req.body.password && req.body.username) {
//		console.log('inserting into table');
//	    pool.query('SELECT * from users where email = $1', [req.body.email], (error, results) => {
//	      if (!error || results) {
//	    	  		
//		    	    pool.query('INSERT INTO users (username, email, password) VALUES ($1, $2, $3)', [req.body.username, req.body.email, bcrypt.hash(req.body.password, null, null, function (err, hash){})], (error, result) => {
//		    	      if (error) {
//		    	        return next(error);
//		    	      } else {
//			    	        	req.session.userId = user._id;
//			    	    	    // Create a verification token for this user
//			    	    	    pool.query('INSERT INTO tokens (token, date) VALUES ($1, $2)', [crypto.randomBytes(16).toString('hex'), new Date()], (error, results) => {});
////			    	            var mailOptions = { from: 'contact@helplicit.com', to: user.email, subject: 'Confirm your registration on FortyQ', text: 'Hello,\n\n' + 'Please verify your account by clicking the link below: \nhttp:\/\/' + req.headers.host + '\/confirmation\/:' + token.token + '&:' + user.email + '\n' };		
////			    	    	    // Save the verification token
////			    	    	    token.save(function (err) {
////			    	    	        if (err) { return res.status(500).send({ msg: err.message }); }
////			    	    	        // Send the email
////			    	    	        transporter.sendMail(mailOptions, function (err, info) {
////			    	    	        	   if(err)
////			    	    	        	     console.log("transport error is " + err);
////			    	    	        	   else
////			    	    	        	     console.log("transport info is " + info);
////			    	    	        	});        
////		//	    	    	        transporter.sendMail(mailOptions, function (err) {
////		//	    	    	            if (err) { return res.status(500).send({ msg: err.message }); }
////		//	    	    	            res.status(200).send('A verification email has been sent to ' + user.email + '.');
////		//	    	    	        });
////			    	    	    });	 
//			    	        	res.render('login.hbs', { success: req.session.success, successMessage: "A confirmation link has been sent to your mailbox", signup: 'signup'});
//		    	      }
//		    	    });
//	      	}
//	    })
//    }
//else {
//    var err = new Error('All fields are required.');
//    let email = req.body.email;
////    req.checkBody('email', 'email is required').notEmpty();    
//    err.status = 400;
//    req.session.errors = errors;
//    req.session.success = false;
//    res.render('login.hbs', { success: req.session.success, err: err, errors: req.session.errors, signup: 'signup'
//    });
//    
////    return next(err);
//  }
//})

//POST route for updating data
router.post('/signup', function (req, res, next) {
	console.log('req.session '+req.session);
	const errors = req.validationResult;
if (req.body.email && req.body.password && req.body.username) {

	pool.query('SELECT * from users where email = $1', [req.body.email], (error, results) => {
      if (!error || results) {
    	  pool.query('INSERT INTO users (username, email, password) VALUES ($1, $2, $3)', [req.body.username, req.body.email, bcrypt.hash(req.body.password, 10, function (err, hash){})], (error, result) => {
	    	      if (error) {
	    	        return next(error);
	    	      } else {
//		    	        	req.session.userId = user._id;
		    	    	    // Create a verification token for this user
		    	            var mailOptions = { from: 'contact@helplicit.com', to: req.body.email, subject: 'Confirm your registration on FortyQ', text: 'Hello,\n\n' + 'Please verify your account by clicking the link below: \nhttp:\/\/' + req.headers.host + '\/confirmation\/:' + token + '&:' + req.body.email + '\n' };		
		    	    	    // Save the verification token
		    	            var token = crypto.randomBytes(16).toString('hex');
		    	            var tokenDate = new Date();
		    	            pool.query('INSERT INTO tokens (token, createdAt) VALUES ($1, $2)', [token, tokenDate], (error, results) => {
		    	    	        if (error) { res.render('404.hbs') }
		    	    	        // Send the email
		    	    	        transporter.sendMail(mailOptions, function (err, info) {
		    	    	        	   if(err)
		    	    	        	     console.log("transport error is " + err);
		    	    	        	   else
		    	    	        	     console.log("transport info is " + info);
		    	    	        	});        
		    	            });		    	            
		    	        	res.render('login.hbs', { success: req.session.success, successMessage: "A confirmation link has been sent to your mailbox", signup: 'signup'});
	    	      }
	    	    });    	  
      } else {
          var err = new Error('Wrong email or password.');
          err.status = 401;
          req.session.errors = errors;
          req.session.success = false;
          res.render('index.hbs', { success: req.session.success, err: err, errors: req.session.errors, signup: 'signup', user: {
              firstname: 'Manish',
              lastname: 'Prakash',
              email: 'manish@excellencetechnologies.in'
          	}
          });
      }
    });
  } else {
    var err = new Error('All fields are required.');
    let email = req.body.email;
//    req.checkBody('email', 'email is required').notEmpty();    
    err.status = 400;
    req.session.errors = errors;
    req.session.success = false;
    res.render('login.hbs', { success: req.session.success, err: err, errors: req.session.errors, signup: 'signup', user: {
        firstname: 'Manish',
        lastname: 'Prakash',
        email: 'manish@excellencetechnologies.in'
    	}
    });
    
  }
})

//router.get(/^\/confirmation\/(\w+)(?:\.\.(\w+))?$/, function(req, res, next){
router.get('/confirmation/:token&:email', function(req, res, next){
	
	  var from = req.params[0];
	  var to = req.params[1] || 'HEAD';
	  var token = req.params.token;
	  token = token.substring(1, token.length);
	  var email = req.params.email;
	  email = email.substring(1, email.length);
	
    // Find a matching token
    pool.query('SELECT * from tokens where token = $1', [token], (err, token) => {
        if (!token) return res.status(400).send({ type: 'not-verified', msg: "The token has expired. Please ask for a new one." });
        // If we found a token, find a matching user
        pool.query('SELECT * from users where email = $1', [email], (err, user) => {
        	if(err) console.log('err is ' + err);
            if (!user) return res.status(400).send({ msg: "No user found for this token." });
            if(user.passwordResetToken){
            	console.log('username is @@@ ' + user.username);
            	return res.render('resetPassword.hbs', {username : user.username});
            	}
            if (user.isVerified) res.render('login.hbs', { success: req.session.success, successMessage: "This user has already been verified."});
            // Verify and save the user
	    	myquery = {email : email};
	    	newValues = {$set : {isVerified : true}};
	    	pool.query('UPDATE users set isVerified = true where email = $1', [email], (err, success) => {
                if (err) { return res.status(500).send({ msg: err.message }); }
	        	res.render('login.hbs', { success: req.session.success, successMessage: "Your registration has been completed. Please sign in."});
            });
        });
    });
});

//GET route for allowing a paralegal to login or sign up
router.get('/login', function (req, res) {

	console.log('in the login function');
	res.render('login', {signup: 'signup'});
});

module.exports = router;