/*jshint esversion: 6 */
var express = require('express');
var path = require("path");
var router = express.Router();
var QAMaster = require('../models/qamaster');
var LawyerProfileMaster = require('../models/lawyerProfileMaster');
var slugify = require('slugify');
var moment = require('moment');
var crypto = require('crypto');
var nodemailer = require('nodemailer');
var i18n = require('i18n');
var bcrypt = require('bcrypt');
var formidable = require('formidable');
var passwordValidator = require('password-validator');
var Constants = require('../constants/constants');
var transporter = nodemailer.createTransport({
	 service: 'zoho',
	 auth: {
	        user: 'contact@helplicit.com',
	        pass: 'grenoblefrance'
	    }
	});
var fs = require('fs');
const multer = require("multer");
const upload = multer({
	  dest: "uploads"
	  // you might also want to set some limits: https://github.com/expressjs/multer#limits
	});
var ssn;
var redis = require("redis");
var client = redis.createClient();
const config = require('../config/config.json');
const defaultConfig = config.development;
const Pool = require('pg').Pool
const pool = new Pool({
  user: defaultConfig.dbUser,
  host: defaultConfig.host,
  database: defaultConfig.database,
  password: defaultConfig.dbPassword,
  port: 5432,
})

//Create a password validator schema
var schema = new passwordValidator();
//Add properties to it
schema
.is().min(8)                                    // Minimum length 8
.is().max(100)                                  // Maximum length 100
.has().uppercase()                              // Must have uppercase letters
.has().lowercase()                              // Must have lowercase letters
.has().digits()                                 // Must have digits


//GET route for reading data, loading the homepage with a list of all GLOs filed
router.get('/', function (req, res) {
	ssn = req.session;
	res.render('index');
    //pull out all the recently answered questions on the platform
/*    QAMaster.find({response : true}, {}, {sort: {'responseDate': -1}, limit: 4}, function (err, qamaster) {
	            if (!qamaster){
	            	console.log('record list not found');
	            } 
	            if(err){
	            	console.log('err ' + err);
	            }
	            else{
	            	console.log('list is found');
	            	var qaList = [];
	            	for(var i = 0; i < qamaster.length; i++){
	            		if (qamaster[i].response == true) {
	            			currentDate = moment().get('date') + 31;//manual tweak to ensure that dates that are separated over a month are calculated as dates from different months	
	            			console.log('$$current date is ' + currentDate);
	            			if(i==0){
	            				dateGap = currentDate - moment(qamaster[i].responseDate).get('date');
	            				console.log('dateGap is ' + dateGap);
	            			}
	            			recordedDate = moment(qamaster[i].responseDate).get('date');
	            			newDate = moment(qamaster[i].responseDate).set('date', recordedDate + dateGap - 1);
    	            		var qaRecord = {
    	            				'subject':qamaster[i].subject,
    	            				'responseDate' : newDate.locale('en').format('lll'),
    	            				'responseBy':qamaster[i].responseBy,
    	            				'photo': qamaster[i].photo
    	            		}
	            		}
	            		console.log('qarecord is '+ qaRecord.photo);
	            		qaList.push(qaRecord);
	            	}
	//	              now pull out records of the most active lawyers on the platform
		                LawyerProfileMaster.find({}, {}, {sort: {'responseCount': -1}, limit: 6}, function (err, lawyerProfile) {
		                    if (!lawyerProfile){
		                    	console.log('record list not found');
		                    } 
		                    if(err){
		                    	console.log('err ' + err);
		                    }
		                    else{
		                    	console.log('lawyer list is found');
		                    	console.log('lawyer record is ' + lawyerProfile.length);		                    	
		                    	var lawyerList = [];
		                    	for(var i = 0; i < lawyerProfile.length; i++){
                        		var lawyerRecord = {
                        				'firstname':lawyerProfile[i].firstname,
                        				'region' : lawyerProfile[i].region,
                        				'photo': lawyerProfile[i].photo,
                        				'username': lawyerProfile[i].username
                        		}
                        			console.log('lawyer record is ' + lawyerProfile.length);
		                    		lawyerList.push(lawyerRecord);
		                    	}
//                    		slice the lawyerList array into two equal arrays
	                    	lawyerList2 = lawyerList.slice(3,6);
	                    	lawyerList = lawyerList.slice(0,3);
		                    res.render('index', {qaList : qaList, lawyerList : lawyerList, lawyerList2 : lawyerList2});
		                    }
		                });	            	
//	            res.render('index', {qaList : qaList});
	            }
    }); */

});


router.get('/lang/:language', function(req, res){
	console.log('req.params.language ' + req.params.language);
	res.cookie('locale', req.params.language.substring(1, req.params.language.length));
	i18n.setLocale(res, req.params.language.substring(1, req.params.language.length));
	res.redirect(req.headers.referer);
});


//GET route for allowing a paralegal to login or sign up
router.get('/login', function (req, res) {

	console.log('in the login function');
	res.render('login', {signup: 'signup'});
});

function requireLogin(req, res, next){
	console.log('req.user', req.user);
    if (!req.session.user) {
            req.session.prevUrl = req.body.url;
            res.redirect('/login');
    }
    else next();
}

//POST route for client login
router.post('/login', function (req, res, next) {
  var errors = req.validationErrors();
  if (req.body.email &&
    req.body.password) {

    // If we found a token, find a matching user
	  pool.query('SELECT * from users where email = $1', [req.body.email], (error, user) => {
		console.log('user.rowCount ', user.rowCount);
        if (error || user.rowCount == 0){
        	console.log('error ' + error);
        	console.log('user ' + user);
        	var err = new Error("User not found");
        	res.render('login.hbs', {success: req.session.success, err: err, signup: 'signup'});
        } 
        else if (!user.rows[0].isverified){
        	var err = new Error("The account has not been verified. Please click on the confirmation link sent" + 
    		"to your mailbox to continue");
        	res.render('login.hbs', { success: req.session.success, successMessage: "Your mail has been verified. Please login", signup: 'signup'});
        }
        else {
        	req.session.email = req.body.email;
        	req.session.user = user.rows[0];
        	pool.query ('SELECT * from qamaster where username = $1', [user.rows[0].username], (err, qamaster) => {
        	            if (!qamaster){
        	            	console.log('record list not found');
        	            } 
        	            if(err){
        	            	console.log('err ' + err);
        	            }
        	            else{
        	            	console.log('list is found');
        	            	var qaList = [];
        	            	for(var i = 0; i < qamaster.rowCount; i++){
            	            		var qaRecord = {
            	            				'subject' : qamaster.rows[i].subject,
            	            				'createdAt' : moment(qamaster.rows[i].createdat).locale('en').format('lll'),
            	            				'slugURL' : qamaster.rows[i].slugurl,
            	            				'startDate' : qamaster.rows[i].startdate,
            	            				'endDate' : qamaster.rows[i].enddate,
            	            				'quoteAmount' : qamaster.rows[i].quoteamount,
            	            				'status' : qamaster.rows[i].status
            	            		}
        	            		qaList.push(qaRecord);
        	            	}
        	            	req.session.username = user.rows[0].username;
//        	            	if(user.isLawyer) res.render('lawyerDashboard', {success: req.session.success, errors: req.session.errors,
//        	            		qaList: qaList, test: 'test'});
//        	            	res.render('lawyerDashboard', {qaList: qaList});
        	            	if(user.rows[0].islawyer && !user.rows[0].lastlogin) {
						    	pool.query('UPDATE users set lastlogin = $1 where email = $2', [moment().locale('en').format('lll'), req.body.email], (err, success) => {
					                if (err) { return res.status(500).send({ msg: err.message }); }
		        	            	res.render('lawyerprofile', {success: req.session.success, errors: req.session.errors,
		        	            		qaList: qaList, test: 'test', username: user.rows[0].username})
					            });    	            		
        	            	}

        	            	//if the user is a lawyer and is logging in to provide a quote 
//    	            		else if(user.rows[0].islawyer && user.rows[0].lastlogin && req.session.slugURL) {
//						    	pool.query('UPDATE users set lastlogin = $1 where email = $2', [moment().locale('en').format('lll'), req.body.email], (err, success) => {
//					                if (err) { return res.status(500).send({ msg: err.message }); }
//					            }); 
//						        pool.query('select * from qamaster where slugURL = $1', [req.session.slugURL], (err, qaRecord) => {
//						            if (!qaRecord){
//						            	console.log('record not found');
//						            	res.render('404');
//						            }
//						            else{res.render('lawyerQuote', {question: qaRecord.rows[0].question, date: moment(qaRecord.rows[0].createdat).locale('en').format('L'), subject: qaRecord.rows[0].subject, username: qaRecord.rows[0].username, answer: qaRecord.rows[0].answer, loggedIn: true});}
//						        });
//    	            		}
        	            	
    	            		else if(user.rows[0].islawyer && user.rows[0].lastlogin) {
						    	pool.query('UPDATE users set lastlogin = $1 where email = $2', [moment().locale('en').format('lll'), req.body.email], (err, success) => {
					                if (err) { return res.status(500).send({ msg: err.message }); }
					            });    	            			
    	            			res.render('lawyerDashboard', {qaList: qaList, username : req.session.username})}
        	            	
        	            	//case when the user is not a lawyer and has asked a quesion
    	            		else if (!user.rows[0].islawyer && req.session.questionasked){
    	            			res.render('newQuestion', {username: user.rows[0].username, questionText: req.session.questionasked});
    	            		}

        	            	//case when the user is not a lawyer and has already logged in the past        	            	
        	            	else {res.render('clientDashboard', {qaList : qaList, username : user.rows[0].username});}
        	            } 
        	        });        	
        }
    });    
  }  else {
    var err = new Error('All fields are required');
    let email = req.body.email;
//    req.checkBody('email', 'email is required').notEmpty();    
    err.status = 400;
    req.session.errors = errors;
    req.session.success = false;
    res.render('login.hbs', { success: req.session.success, err: err, errors: req.session.errors, signin: 'signin'
    });
    
//    return next(err);
  }
})

//POST route for updating data
router.post('/signup', function (req, res, next) {
	console.log('req.session '+req.session);
  // confirm that user typed same password twice
//  if (req.body.password !== req.body.passwordConf) {
//    var err = new Error('Passwords do not match.');
//    err.status = 400;
//    res.send("passwords dont match");
//    return next(err);
//  }

//  let email = req.body.email;
//  req.checkBody('email', 'email is required').notEmpty();
	const errors = req.validationResult;
//  if(errors){
//	 console.log('errors'+errors);
//     req.session.errors = errors;
//     req.session.success = false;
//     res.setHeader('Cache-Control', 'no-cache');
//     res.render('index.hbs', { success: req.session.success, errors: req.session.errors, firstname: 'ankuj', user: {
//         firstname: 'Manish',
//         lastname: 'Prakash',
//         email: 'manish@excellencetechnologies.in'
//     	}
//     });
////     res.redirect('/');
//  }

if (req.body.email && schema.validate(req.body.password) && req.body.username) {

	pool.query('SELECT * from users where email = $1', [req.body.email], (error, user) => {
	hashedPassword = bcrypt.hashSync(req.body.password, 10)
      if (error || user.rowCount==0) {

  	    var userData = {
	    	      email: req.body.email,
	    	      username: req.body.username,
	    	      password: req.body.password,
	    	      isLawyer: Boolean(req.body.isLawyer)
	    	    }
//  	    		var user = User({email: req.body.email, username: req.body.username, password: req.body.password});
  	    		  pool.query('INSERT INTO users (username, email, password, lastLogin) VALUES ($1, $2, $3, $4)', [req.body.username, req.body.email, bcrypt.hashSync(req.body.password, 10), new Date()], (error, result) => {
	    	      if (error) {
	    	        return next(error);
	    	      } else {
		    	        	req.session.userId = user._id;
		    	    	    // Create a verification token for this user
		    	    	    var token = crypto.randomBytes(16).toString('hex');
		    	            var mailOptions = { from: 'contact@helplicit.com', to: req.body.email, subject: 'Confirm your registration on FortyQ', text: 'Hello,\n\n' + 'Please verify your account by clicking the link below: \nhttp:\/\/' + req.headers.host + '\/confirmation\/:' + token + '&:' + req.body.email + '\n' };		
		    	    	    // Save the verification token
		    	            pool.query('INSERT INTO tokens (token, createdAt) VALUES ($1, $2)', [token, new Date()], (error, results) => {
		    	    	        if (err) { return res.status(500).send({ msg: err.message }); }
		    	    	        // Send the email
		    	    	        transporter.sendMail(mailOptions, function (err, info) {
		    	    	        	   if(err)
		    	    	        	     console.log("transport error is " + err);
		    	    	        	   else
		    	    	        	     console.log("transport info is " + info);
		    	    	        	});        
	//	    	    	        transporter.sendMail(mailOptions, function (err) {
	//	    	    	            if (err) { return res.status(500).send({ msg: err.message }); }
	//	    	    	            res.status(200).send('A verification email has been sent to ' + user.email + '.');
	//	    	    	        });
		    	    	    });	 
		    	        	res.render('login.hbs', { success: req.session.success, successMessage: "A confirmation link has been sent to your mailbox", signup: 'signup'});
	    	      }
	    	    });    	  
      } else {
          var err = new Error('Email already exists');
          err.status = 401;
          req.session.errors = errors;
          req.session.success = false;
          res.render('index.hbs', { success: req.session.success, err: err, errors: req.session.errors, signup: 'signup', user: {
              firstname: 'Manish',
              lastname: 'Prakash',
              email: 'manish@excellencetechnologies.in'
          	}
          });
      }
    });
  } else {
    var err = new Error('Please make sure that all fields are filled. Password must mave a min length of 8 and contain uppercase and lowercase letters.');
    let email = req.body.email;
//    req.checkBody('email', 'email is required').notEmpty();    
    err.status = 400;
    req.session.errors = errors;
    req.session.success = false;
    res.render('login.hbs', { success: req.session.success, err: err, errors: req.session.errors, signup: 'signup'});
    
//    return next(err);
  }
})


//POST route for updating data
router.post('/signupLawyer', function (req, res, next) {
	console.log('req.session '+req.session);
		const errors = req.validationResult;
	if (req.body.email && schema.validate(req.body.password) && req.body.username) {
		pool.query('SELECT * from users where email = $1', [req.body.email], (error, user) => {
			console.log('user is ', user);
			console.log('error is ', error);
			hashedPassword = bcrypt.hashSync(req.body.password, 10)
			console.log('password is ', hashedPassword);		
	      if (error || user.rowCount==0) {
	  	    		  pool.query('INSERT INTO users (username, email, password, islawyer) VALUES ($1, $2, $3, $4)', [req.body.username, req.body.email, bcrypt.hashSync(req.body.password, 10), true], (error, result) => {
		    	      if (error) {
		    	        return next(error);
		    	      } else {
			    	        	req.session.userId = user._id;
			    	    	    // Create a verification token for this user
			    	    	    var token = crypto.randomBytes(16).toString('hex');
			    	            var mailOptions = { from: 'contact@helplicit.com', to: req.body.email, subject: 'Confirm your registration on FortyQ', text: 'Hello,\n\n' + 'Please verify your account by clicking the link below: \nhttp:\/\/' + req.headers.host + '\/confirmation\/:' + token + '&:' + req.body.email + '\n' };		
			    	    	    // Save the verification token
			    	            pool.query('INSERT INTO tokens (token, createdAt) VALUES ($1, $2)', [token, new Date()], (error, results) => {
			    	    	        if (err) { return res.status(500).send({ msg: err.message }); }
			    	    	        // Send the email
			    	    	        transporter.sendMail(mailOptions, function (err, info) {
			    	    	        	   if(err)
			    	    	        	     console.log("transport error is " + err);
			    	    	        	   else
			    	    	        	     console.log("transport info is " + info);
			    	    	        	});        
		//	    	    	        transporter.sendMail(mailOptions, function (err) {
		//	    	    	            if (err) { return res.status(500).send({ msg: err.message }); }
		//	    	    	            res.status(200).send('A verification email has been sent to ' + user.email + '.');
		//	    	    	        });
			    	    	    });	 
			    	        	res.render('loginLawyer', { success: req.session.success, successMessage: "A confirmation link has been sent to your mailbox", signup: 'signup'});
		    	      }
		    	    });    	  
	      } else {
	          var err = new Error('Email already exists');
	          err.status = 401;
	          req.session.errors = errors;
	          req.session.success = false;
	          res.render('loginLawyer.hbs', { success: req.session.success, err: err, errors: req.session.errors, signup: 'signup', user: {
	              firstname: 'Manish',
	              lastname: 'Prakash',
	              email: 'manish@excellencetechnologies.in'
	          	}
	          });
	      }
	    });
	  } else {
	    var err = new Error('Please make sure that all fields are filled. Password must mave a min length of 8 and contain uppercase and lowercase letters.');
	    let email = req.body.email;
//	    req.checkBody('email', 'email is required').notEmpty();    
	    err.status = 400;
	    req.session.errors = errors;
	    req.session.success = false;
	    res.render('loginLawyer.hbs', { success: req.session.success, err: err, errors: req.session.errors, signup: 'signup'});
	    
//	    return next(err);
	  }
})

//router.get(/^\/confirmation\/(\w+)(?:\.\.(\w+))?$/, function(req, res, next){
router.get('/confirmation/:token&:email', function(req, res, next){
	
	  var from = req.params[0];
	  var to = req.params[1] || 'HEAD';
	  var token = req.params.token;
	  token = token.substring(1, token.length);
	  var email = req.params.email;
	  email = email.substring(1, email.length);
	
    // Find a matching token
	pool.query('SELECT * from tokens where token = $1', [token], (err, token) => {
        if (!token) return res.status(400).send({ type: 'not-verified', msg: "The token has expired. Please ask for a new one." });
        // If we found a token, find a matching user
        pool.query('SELECT * from users where email = $1', [email], (err, user) => {
        	if(err) console.log('err is ' + err);
            if (!user) return res.status(400).send({ msg: "No user found for this token." });
            if(user.rows[0].passwordresettoken){
            	return res.render('resetPassword.hbs', {username : user.rows[0].username});
            	}
            if (user.rows[0].isVerified) res.render('login.hbs', { success: req.session.success, successMessage: "This user has already been verified."});
            // Verify and save the user
	    	myquery = {email : email};
	    	newValues = {$set : {isVerified : true}};
	    	pool.query('UPDATE users set isVerified = $1 where email = $2', [true, email], (err, success) => {
                if (err) { return res.status(500).send({ msg: err.message }); }
	        	res.render('login.hbs', { success: req.session.success, successMessage: "Your registration has been completed. Please sign in."});
            });
        });
    });
});

//GET route for rendering the question asking page
router.get('/askQuestion', function (req, res) {
	res.render('askQuestion');
});

//GET route for login
router.get('/login', function (req, res) {
	res.render('login');
});

//POST to save the question asked by the user
router.post('/confirmQuestion', function(req, res, next){
	console.log('QUESTION: ' + req.body.editordata);
	console.log('EMAIL: ' + req.body.email);
	console.log('SUBJECT: ' + req.body.subject);
	console.log('Date ' + moment().locale('en').format('lll'));
	console.log('slugify' + slugify(req.body.subject));
	console.log('username ' + req.session.username);
	console.log('Constants.OPEN ' + Constants.OPEN);
	var dateTimeNow = moment().locale('en').format('lll');
    // A fresh question is being inserted, add a new record
    pool.query('INSERT INTO qamaster (question, subject, username, slugURL, createdAt, status) VALUES ($1, $2, $3, $4, $5, $6)', [req.body.editordata, req.body.subject, req.session.username, slugify(req.body.subject), dateTimeNow, Constants.OPEN], (err, result) => {
        if (err) { res.send(new Error(err));
        console.log('err is ' + err);
        }	
        else {
        req.session.slugURL = slugify(req.body.subject);
        //send email to the client
//        var mailOptions = { from: 'contact@helplicit.com', to: req.session.email, subject: 'Your legal question', text: 'Hello,\n\n' + 'Thanks for your query, you can track the status of your query at: \n' + 'http:\/\/' + req.headers.host + '\/askQuestion\/:' + slugify(req.body.subject) + '\n Regards \n Team FortyQ \n' };
//        // Send the email
//        transporter.sendMail(mailOptions, function (err, info) {
//        	   if(err)
//        	     console.log(err)
//        	   else
//        	     console.log(info);
//        	}); 
        //send email to lawyer list
        var mailList = ['ankuj@helplicit.com'
//                        'alicemacris@yahoo.co.uk',
//                        'suziharley1530@gmail.com',
//                        'barrister.n.khan@gmail.com',
                        ];

//    	res.render('post', {question: req.body.editordata, date: dateTimeNow, subject: req.body.subject, username: req.session.username, loggedIn: true});
        var mailOptions = { from: 'contact@helplicit.com', bcc: mailList, subject: '[FortyQ] New legal query', text: 'Dear expert,\n' + 'Hope you are well and keeping safe. A new query has come in, details are below: \n' + req.body.editordata + '\n Please provide your quotes and descriptions.\n We look forward to hearing from you.\n Regards \n Team FortyQ \n' };
        req.session.questionasked=null;        
        res.redirect('/clientDashboard/:questionPosted');
        // Send the email
        transporter.sendMail(mailOptions, function (err, info) {
        	   if(err)
        	     console.log(err)
        	   else
        	     console.log(info);
        	}); 
        }
    });    
});

//POST for responding to a question
router.post('/responseToQuestion', function(req, res, next){
	console.log('req.session.slugURL ' + req.session.slugURL);
    pool.query('SELECT * from qamaster where slugURL = $1', [req.session.slugURL], function (err, qaRecord) {
        if (!qaRecord){
        	console.log('err');
        	res.render('404');
        } 
        if(err){
        	console.log('err');
        	res.render('404');
        }
        else{
        	if(qaRecord.username == req.session.username){//user is modifying their question
            	myquery = {slugURL : req.session.slugURL};
            	newValues = {$set : {question : req.body.editordata, createdAt : moment().locale('en').format('lll')}};
		    	pool.query('UPDATE qamaster set question = $1, createdAt = $2 where slugURL = $3', [req.body.editordata, moment().locale('en').format('lll'), req.session.slugURL], (err, success) => {
                    if (err) { return res.status(500).send({ msg: err.message }); }});
                // Send the email
                pool.query('SELECT * from users where username = $1', [qaRecord.username], function (err, user) {
                	
                	if(err){console.log("err is " + err); res.render('404');}
                	else{
                        var mailList = ['contact@helplicit.com',
//                                        'alicemacris@yahoo.co.uk',
//                                        'suziharley1530@gmail.com',
//                                        'barrister.n.khan@gmail.com',
                                        user.email
                                        ];                		
//                        var mailOptions = { from: 'contact@helplicit.com', bcc: mailList, subject: 'Your legal question', text: 'Hello,\n\n' + 'Your question has been answered at: \n' + 'www.fortyq.com/askQuestion/:' + req.session.slugURL + '\n Regards \n Team FortyQ \n' };
//                        transporter.sendMail(mailOptions, function (err, info) {
//                     	   if(err)
//                     	     console.log(err)
//                     	   else
//                     	     console.log(info);
//                     	});                 		
                	}
                });            	
            	res.render('post', {question: req.body.editordata, date: qaRecord.createdAt, subject: qaRecord.subject, username: qaRecord.username, loggedIn: true});
        	}
        	else{
            	myquery = {slugURL : req.session.slugURL};
            	newValues = {$set : {answer : req.body.editordata, response : true, responseBy : req.session.username, responseDate : moment().locale('en').format('lll')}};
            	pool.query('UPDATE qamaster set answer = $1, response = $2, responseBy = $3, responseDate = $4 where slugURL = $5', [req.body.editordata, true, req.session.username, moment().locale('en').format('lll'), req.session.slugURL], (err, success) => {
                    if (err) { return res.status(500).send({ msg: err.message }); }});        	
                // Send the email
                pool.query('select * from USERS where username = $1', [qaRecord.username], (err, user) => {
                	
                	if(err){console.log("err is " + err); res.render('404');}
                	else{
                        var mailList = ['contact@helplicit.com',
//                                        'alicemacris@yahoo.co.uk',
//                                        'suziharley1530@gmail.com',
//                                        'barrister.n.khan@gmail.com',
                                        user.email
                                        ];               		
//                        var mailOptions = { from: 'contact@helplicit.com', bcc: mailList, subject: 'Your legal question', text: 'Hello,\n\n' + 'Your question has been answered at: \n' + 'www.fortyq.com/askQuestion/:' + req.session.slugURL + '\n Regards \n Team FortyQ \n' };
//                        transporter.sendMail(mailOptions, function (err, info) {
//                     	   if(err)
//                     	     console.log(err)
//                     	   else
//                     	     console.log(info);
//                     	});                 		
                	}
                });
            	
            	res.render('post', {question: qaRecord.question, date: qaRecord.createdAt, subject: qaRecord.subject, username: qaRecord.username, answer: req.body.editordata, responseDate: qaRecord.responseDate, responseBy: req.session.username, loggedIn: true});
        	}
//            var mailOptions = { from: 'contact@helplicit.com', to: req.session.email, subject: 'Your legal question', text: 'Hello,\n\n' + 'Thanks for your question, you can follow the status of the response at the following link: \n' + 'http://www.helplicit.com/askQuestion/:' + slugify(req.session.slugURL) + '\n Team SuperLawyer \n' };
//            // Send the email
//            transporter.sendMail(mailOptions, function (err, info) {
//            	   if(err)
//            	     console.log(err)
//            	   else
//            	     console.log(info);
//            	}); 
        	
        	//req.session.errors = null;
        } 
    });        
});


//GET for creating a new GLO
router.get('/createQuestion', function (req, res, next) {
	res.render('newQuestion');
});

//GET route for allowing the client to view the question they asked, alongside the quotes being provided by the lawyers
router.get('/askquestion/:slug&:additionalMessage', function (req, res) {
	req.session.slugURL = req.params.slug.substring(1, req.params.slug.length);
	slugURL = req.params.slug.substring(1, req.params.slug.length);
	message =  req.params.additionalMessage.substring(1, req.params.additionalMessage.length);
	console.log('message ', message);
	var successMessage;
	if (message.includes('Success')){successMessage = "An email has been sent to request the lawyer for a 15 minute intro."};
    // find the GLO with the matching ID
    pool.query('select * from qamaster where slugurl = $1', [slugURL], (err, qaRecord) => {
        if (!qaRecord){
        	console.log('record not found');
        	res.render('404', {error:err});
        } 
        else if(err){
        	console.log('err ' + err);
        	res.render('404', {error:err});
        }
        else{
        	console.log('qaRecord.rows[0].id ', qaRecord.rows[0].id);
        	var quoteList = [];
        	pool.query('select * from lawyerquotemaster where quoteid = $1 and quoterejected = $2', [qaRecord.rows[0].id, false], (err, lawyerQuoteRecordSet) => {
                if(err){
                	console.log('err ' + err);
                	res.render('404', {error:err});
                }
                else {
	                	for(var i = 0; i < lawyerQuoteRecordSet.rowCount; i++){
		            		var quoteRecord = {
		            				'quoteValue':lawyerQuoteRecordSet.rows[i].quotevalue,
		            				'quoteID' : lawyerQuoteRecordSet.rows[i].quoteid
		            		}
	            		quoteList.push(quoteRecord);
	            	} 
        		}
        	});
     	        if (!req.session.username) { res.render('post', {question: qaRecord.rows[0].question, date: moment(qaRecord.rows[0].createdat).locale('en').format('lll'), subject: qaRecord.rows[0].subject, username: qaRecord.rows[0].username, answer: qaRecord.rows[0].answer, responseBy: qaRecord.rows[0].responseBy, responseDate: moment(qaRecord.rows[0].responseDate).locale('en').format('lll'), loggedIn: false, quoteList : quoteList, successMessage : successMessage}); }
     	        res.render('post', {question: qaRecord.rows[0].question, date: moment(qaRecord.rows[0].createdAt).locale('en').format('lll'), subject: qaRecord.rows[0].subject, username: qaRecord.rows[0].username, answer: qaRecord.rows[0].answer, responseBy: qaRecord.rows[0].responseBy, responseDate: moment(qaRecord.rows[0].responseDate).locale('en').format('lll'), loggedIn: true, quoteList : quoteList, successMessage : successMessage});     	        
        	}
        })        	
        	//req.session.errors = null;
});

//GET route for responding to a question/case with a quote
router.get('/quote/:slug', requireLogin, function (req, res) {
	req.session.slugURL = req.params.slug.substring(1, req.params.slug.length);
    // find the case with the matching ID, load the case dashboard in case the request has been accepted by the client
    pool.query('select * from qamaster where slugURL = $1', [req.params.slug.substring(1, req.params.slug.length)], (err, qaRecord) => {
        if (!qaRecord){
        	console.log('record not found');
        	res.render('404');
        } 
        else if(err){
        	console.log('err ' + err);
        	res.render('404');
        }
        else{
        		console.log('req.headers.host' + req.headers.host);
        		//redirect lawyer to login first
//        		res.render('login');
//    	        if (!req.session.username) { res.render('post', {question: qaRecord.question, date: moment(qaRecord.createdAt).locale('en').format('lll'), subject: qaRecord.subject, username: qaRecord.username, answer: qaRecord.answer, responseBy: qaRecord.responseBy, responseDate: moment(qaRecord.responseDate).locale('en').format('lll'), loggedIn: false}); }
        		if(qaRecord.rows[0].status == Constants.QUOTE_ACCEPTED){
        			res.render('caseDashboard');
        		}
        		else res.render('lawyerQuote', {question: qaRecord.rows[0].question, date: moment(qaRecord.rows[0].createdAt).locale('en').format('lll'), subject: qaRecord.rows[0].subject, username: qaRecord.rows[0].username, answer: qaRecord.rows[0].answer, responseBy: qaRecord.rows[0].responseby, responseDate: moment(qaRecord.rows[0].responsedate).locale('en').format('lll'), loggedIn: true});	
        	}
        })        	
        	//req.session.errors = null;
});

//GET route for opening up the lawyer profile
router.get('/lawyerProfilePage/:username', function (req, res) {
	lawyerUsername = req.params.username.substring(1, req.params.username.length)
    // find the GLO with the matching ID
    pool.query('SELECT * from lawyerprofilemaster where username = $1', [lawyerUsername], (err, record) => {
        if(err){
        	console.log('err ' + err);
        	res.render('404');
        }
        else if (record == null){
        	console.log('err ' + err);
        	res.render('404');
        }
        else{
        	res.render('lawyerProfilePage.hbs', {firstname : record.firstname, lastname : record.lastname, photo : "/img/" + record.firstname + record.lastname + ".png",
        		linkedinProfile: record.linkedinProfile, areasOfPractice: record.areasOfPractice, languageSpoken : record.languageSpoken, pricePerHour : record.pricePerHour, priceOfFirstMeeting : record.priceOfFirstMeeting, workTiming : record.workTiming, selectDay1 : record.selectDay1,
    	    	selectStartTime1 : record.selectStartTime1, selectEndTime1 : record.selectEndTime1, selectDay2 : record.selectDay2, selectStartTime2 : record.selectStartTime2, 
    	    	selectEndTime2 : record.selectEndTime2, selectDay3 : record.selectDay3, selectStartTime3 : record.selectStartTime3, selectEndTime3 : record.selectEndTime3,
    	    	selectDay4 : record.selectDay4, selectStartTime4 : record.selectStartTime4, selectEndTime4 : record.selectEndTime4,
    	    	selectDay5 : record.selectDay5, selectStartTime5 : record.selectStartTime5, selectEndTime5 : record.selectEndTime5,
    	    	graduationDate : record.graduationDate, graduationUniversity : record.graduationUniversity, address : record.address
    	    	, city : record.address, region : record.region, city : record.city, professionalExperience : record.professionalExperience, username : record.username });		    			
        }
        	//req.session.errors = null;
    });	
});


//GET page which shows the questions that a lawyer has responded to so far
router.get('/questionsRespondedSoFar/:username', function (req, res) {
	console.log('username is &&&' + req.params.username.substring(1, req.params.username.length));
    // find the GLO with the matching ID
    pool.query('SELECT * from qamaster where responseBy = $1', [req.params.username.substring(1, req.params.username.length)], (err, records) => {
        if (!records){
        	console.log('record not found');
        	res.render('404');
        } 
        else if(err){
        	console.log('err ' + err);
        	res.render('404');
        }
        	console.log('the records are ' + records);
        	res.render('lawyerProfileAllResponses', {records:records});
        	//req.session.errors = null;
    });	
});

router.get('/setLanguage/:language', function(req, res){
	console.log('req.params.language ' + req.params.language);
	res.cookie('locale', req.params.language.substring(1, req.params.language.length));
	i18n.setLocale(res, req.params.language.substring(1, req.params.language.length));
	res.redirect(req.headers.referer);
});

//GET /logout
router.get('/logout', function(req, res, next) {
  if (req.session) {
    // delete session object
    req.session.destroy(function(err) {
      if(err) {
        return next(err);
      } else {
        return res.redirect('/');
      }
    });
  }
});


//GET for accessing the forgot password page
router.get('/forgotPassword', function (req, res, next) {
	console.log('in the forgot password function');
	res.render('forgotPassword');
});

//POST for the case of a forgot password, sending the confirmation link to mailbox of a registered user
router.post('/forgotPasswordMailboxLink', function (req, res, next) {
	console.log('in the forgot password function with email id ' + req.body.email);
	//	check if email is empty and return that as an error
	if(!req.body.email){
	    var err = new Error('Please fill in the email ID');
    	res.render('forgotPassword', {err: err});
    }
	//look for the email ID in the database, send error message in case it isn't present
    pool.query('SELECT * from users where email = $1', [req.body.email], (err, user) => {
        if (!user){
    	    var err = new Error('Email ID not found');
        	res.render('forgotPassword', {err: err});
        } 
        else{
        	req.session.userId = user._id;
    	    // Create a verification token for this user
    	    var token = crypto.randomBytes(16).toString('hex');
	    	pool.query('UPDATE users set passwordResetToken = $1 where email = $2', [token, req.body.email], (err, success) => {
                if (err) { return res.status(500).send({ msg: err.message }); }
                if (success) {console.log('the table has been updated');}
            });  
            var mailOptions = { from: 'contact@helplicit.com', to: req.body.email, subject: 'Account Verification Token', text: 'Hello,\n\n' + 'Please verify your account by clicking the link: \nhttp:\/\/' + req.headers.host + '\/confirmation\/:' + token + '&:' + req.body.email + '.\n' };		
    	    // Save the verification token
    	    pool.query('INSERT into tokens(token, createdAt) VALUES ($1, $2)', [token, new Date()], (err, success) => {
    	        if (err) { res.render('404', {error:err}); }
    	        // Send the email
    	        transporter.sendMail(mailOptions, function (err, info) {
    	        	   if(err)
    	        	     console.log(err)
    	        	   else
    	        	     console.log(info);
    	        	});
    	    });	        	
        	res.render('forgotPassword.hbs', { success: req.session.success, successMessage: "A confirmation link has been sent to your mailbox"});
        } 
    });		
});


//POST for the case of a reset password
router.post('/reset', function (req, res, next) {
	console.log('req.body.username ' + req.body.username);
	//	check if email is empty and return that as an error
	if(schema.validate(req.body.password) && schema.validate(req.body.confirmPassword)){
		if(req.body.password!=req.body.confirmPassword){
		    var err = new Error('Passwords should match, and must have a min length of 8 and contain uppercase and lowercase letters.');
	    	return res.render('resetPassword', {err: err});
		}
	}
	else{
	    var err = new Error('Please fill in all fields. Password must have a min length of 8 and contain uppercase and lowercase letters.');
    	return res.render('resetPassword', {err: err});
	}
	bcrypt.hash(req.body.password, 10, function (err, hash) {
		    if (err) {
		    	console.log('err$$' + err);
		    }
	    	pool.query('UPDATE users set password = $1 where username = $2', [hash, req.body.username], (err, success) => {
	    		if (err) res.send(err);
	    	    //pull out all the GLOs that a certain user has created to display on the dashboard
	    		pool.query('SELECT * from users where username = $1', [req.body.username], (err, user) => {	    		
	    	    	if(user.rows[0].islawyer){
	    	    		pool.query('SELECT * from qamaster where response = false', (err, success) => {
    	                QAMaster.find({response : false}, function (err, qamaster) {
            	            if (!qamaster){
            	            	console.log('record list not found');
            	            } 
            	            if(err){
            	            	console.log('err ' + err);
            	            }
            	            else{
            	            	console.log('list is found');
            	            	var qaList = [];
            	            	for(var i = 0; i < qamaster.length; i++){
            	            		if (qamaster[i].response == false) {
                	            		var qaRecord = {
                	            				'subject':qamaster[i].subject,
                	            				'createdAt' : qamaster[i].createdAt,
                	            				'slugURL':qamaster[i].slugURL,
                	            				'question': qamaster[i].question
                	            		}
            	            		}
            	            		console.log('qarecord is '+ qaRecord);
            	            		qaList.push(qaRecord);
            	            	}
            	            	req.session.username = user.username;
            	                res.render('lawyerDashboard', {qaList: qaList, username : user.username});
            	            } 
            	        });
	    	    	});
	    	    	}
	            	else res.render('newQuestion', {username: user.username});     	    	    	
	    	});            	
		  })
	//look for the email ID in the database, send error message in case it isn't present
//    User.findOne({email: req.body.email}, function (err, user) {
//        if (!user){
//    	    var err = new Error('Email ID not found');
//        	res.render('forgotPassword', {err: err});
//        } 
//        else{
//        	req.session.userId = user._id;
//        	res.render('forgotPassword.hbs', { success: req.session.success, successMessage: "A confirmation link has been sent to your mailbox"});
//    	    // Create a verification token for this user
//    	    var token = Token({ _userId: user._id, token: crypto.randomBytes(16).toString('hex') });
//    	    user.passwordResetToken = token.token;
//            User.updateOne({passwordResetToken : token.token}, function (err) {
//                if (err) { return res.status(500).send({ msg: err.message }); }
//            });    	    
//            var mailOptions = { from: 'ankuj@helplicit.com', to: req.body.email, subject: 'Account Verification Token', text: 'Hello,\n\n' + 'Please verify your account by clicking the link: \nhttp:\/\/' + req.headers.host + '\/confirmation\/:' + token.token + '&:' + user.email + '.\n' };		
//    	    // Save the verification token
//    	    token.save(function (err) {
//    	        if (err) { return res.status(500).send({ msg: err.message }); }
//    	        // Send the email
//    	        transporter.sendMail(mailOptions, function (err, info) {
//    	        	   if(err)
//    	        	     console.log(err)
//    	        	   else
//    	        	     console.log(info);
//    	        	});        
////	    	    	        transporter.sendMail(mailOptions, function (err) {
////	    	    	            if (err) { return res.status(500).send({ msg: err.message }); }
////	    	    	            res.status(200).send('A verification email has been sent to ' + user.email + '.');
////	    	    	        });
//    	    });	        	
//        } 
//    });		
});
});

//GET route for blog post
router.get('/askQuestionWrite', function (req, res) {
	res.render('confirmQuestion');
});

//GET route for blog post
router.get('/covid', function (req, res) {
	res.render('corona');
});

//GET route for lawyer profile form filling
router.get('/lawyerProfile', function (req, res) {
	res.render('lawyerProfile');
});

//GET route for lawyer profile page viewing
router.get('/lawyerProfilePage', function (req, res) {
	res.render('lawyerProfilePage');
});

//GET route for lawyer profile page viewing
router.get('/loginLawyer', function (req, res) {
	res.render('loginLawyer', {signup:true});
});

//GET route for helplicit enterprise viewing
router.get('/indexEn', function (req, res) {
	res.render('indexEn');
});

//GET route for helplicit enterprise viewing
router.get('/termsconditions', function (req, res) {
	res.render('termsConditions');
});

//code for lawyer profile submit page
router.post('/submitLawyerProfessionalDetails', function (req, res) {
	console.log('moment(req.body.graduationDate)', moment(req.body.graduationDate, 'YYYY-MM-DD'));
	if(req.body.firstname && req.body.lastname && req.body.areasOfPractice && req.body.linkedinProfile && req.body.languageSpoken && req.body.graduationDate && req.body.graduationUniversity && req.body.country  && req.body.city && 
			req.body.professionalExperience){
	      const tempPath = path.join(path.resolve('./'), "./views/img/image.png");
	      const targetPath = path.join(path.resolve('./'), "./views/img/" + req.body.firstname + req.body.lastname + ".png");
	      fs.rename(tempPath, targetPath, function(err) {
	        if (err) console.log('error is ' + err);
	      });
	      	//if the record is being modified
		    if(req.session.modify){
		    	pool.query('UPDATE lawyerprofilemaster set firstname = $1, lastname = $2, photo = $3, linkedinProfile = $4, areasOfPractice = $5, languageSpoken = $6, graduationDate = $7, graduationUniversity = $8, country = $9, city = $10, professionalExperience= $11, username = $12 where username = $13', [req.body.firstname, req.body.lastname, "/img/" + req.body.firstname + req.body.lastname + ".png",
                req.body.linkedinProfile, req.body.areasOfPractice, req.body.languageSpoken, req.body.graduationDate, req.body.graduationUniversity, req.body.country, req.body.city, req.body.professionalExperience, req.session.username, req.session.username], (err, lawyer) => {
		    		if(err){console.log('error is ' + err);res.render('404.hbs');}
		    		else {
			        	res.render('lawyerProfilePage', {firstname : req.body.firstname, lastname : req.body.lastname, photo : "/img/" + req.body.firstname + req.body.lastname + ".png",
			        		linkedinProfile: req.body.linkedinProfile, areasOfPractice: req.body.areasOfPractice, languageSpoken : req.body.languageSpoken, pricePerHour : req.body.pricePerHour, 
			        		graduationDate : req.body.graduationDate, graduationUniversity : req.body.graduationUniversity, address : req.body.address
			    	    	, professionalExperience : req.body.professionalExperience, username : req.session.username });		    			
		    		}
		    			
		    	})
		    }
		    // A new profile is being added
		    pool.query('INSERT into lawyerprofilemaster(firstname, lastname, photo, linkedinProfile, areasOfPractice, languageSpoken, graduationDate, graduationUniversity, professionalExperience, username, country, city, createdAt) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13)', [
	        req.body.firstname, req.body.lastname, "/img/" + req.body.firstname + req.body.lastname + ".png", req.body.linkedinProfile, req.body.areasOfPractice, req.body.languageSpoken, req.body.graduationDate, req.body.graduationUniversity, req.body.professionalExperience, req.session.username, req.body.country, req.body.city, new Date()], (err) =>{
		        if (err) { res.send(new Error(err));
		        console.log('err is ' + err);
		        }	
		        else {
			            //pull out all the GLOs that a certain user has created to display on the dashboard
//			            QAMaster.find({response : false}, function (err, qamaster) {
//			        	            if (!qamaster){
//			        	            	console.log('record list not found');
//			        	            } 
//			        	            if(err){
//			        	            	console.log('err ' + err);
//			        	            }
//			        	            else{
//			        	            	console.log('list is found');
//			        	            	var qaList = [];
//			        	            	for(var i = 0; i < qamaster.length; i++){
//			        	            		if (qamaster[i].response == false) {
//			            	            		var qaRecord = {
//			            	            				'subject':qamaster[i].subject,
//			            	            				'createdAt' : qamaster[i].createdAt,
//			            	            				'slugURL':qamaster[i].slugURL,
//			            	            				'question': qamaster[i].question
//			            	            		}
//			        	            		}
//			        	            		console.log('qarecord is '+ qaRecord);
//			        	            		qaList.push(qaRecord);
//			        	            	}
//			        	            	res.render('lawyerDashboard', {success: req.session.success, errors: req.session.errors,
//			    	            		qaList: qaList, test: 'test'});
//			        	            } 
//			        	        });
		        	res.render('lawyerProfilePage.hbs', {firstname : req.body.firstname, lastname : req.body.lastname, photo : "/img/" + req.body.firstname + req.body.lastname + ".png",
		        		linkedinProfile: req.body.linkedinProfile, areasOfPractice: req.body.areasOfPractice, languageSpoken : req.body.languageSpoken,
		    	    	graduationDate : req.body.graduationDate, graduationUniversity : req.body.graduationUniversity, country : req.body.country
		    	    	, city : req.body.city, professionalExperience : req.body.professionalExperience, username : req.session.username });		        	
		        }
		    });		
	}
	else{
    	var err = new Error("Please fill in all fields");
    	req.session.modify = true;
    	res.render('lawyerprofile.hbs', {err: err, firstname : req.body.firstname, lastname : req.body.lastname, 
    		linkedinProfile: req.body.linkedinProfile, graduationDate : moment(req.body.graduationDate), graduationUniversity : req.body.graduationUniversity, city : req.body.city
	    	, country : req.body.country, professionalExperience : req.body.professionalExperience, username : req.session.username });		
	}
});

//router.get('/questionDashboard', function(req, res){
//	QAMaster.find({response : false}, function (err, qamaster) {
//	    if (!qamaster){
//	    	console.log('record list not found');
//	    	res.render('404.hbs');
//	    } 
//	    if(err){
//	    	console.log('err ' + err);
//	    	res.render('404.hbs');
//	    }
//	    else{
//	    	console.log('list is found');
//	    	var qaList = [];
//	    	for(var i = 0; i < qamaster.length; i++){
//	    		if (qamaster[i].response == false) {
//	        		var qaRecord = {
//	        				'subject':qamaster[i].subject,
//	        				'createdAt' : qamaster[i].createdAt,
//	        				'slugURL':qamaster[i].slugURL,
//	        				'question': qamaster[i].question
//	        		}
//	    		}
//	    		qaList.push(qaRecord);
//	    	}
//			res.render('lawyerDashboard', {qaList: qaList, username : req.session.username})};
//	});	
//});



//GET route for lawyer profile page editing
router.get('/editLawyerProfile', function (req, res) {
	pool.query('SELECT * from lawyerprofilemaster where username = $1', [req.session.username], (err, lawyer) => {
		//function to edit lawyer profile
		if (err) {res.render('404.hbs');}
		else if (lawyer==null){res.render('404.hbs', {error:err});}
		else {
			res.render('lawyerProfile.hbs', {firstname : lawyer.firstname, lastname : lawyer.lastname, photo : lawyer.photo,
				linkedinProfile: lawyer.linkedinProfile, 
		    	pricePerHour : lawyer.pricePerHour, graduationDate : lawyer.graduationDate, graduationUniversity : lawyer.graduationUniversity, professionalExperience : lawyer.professionalExperience, username : lawyer.username });			
		}
	})
	
});


//GET route for lawyer profile page viewing as seen publicly
router.get('/lawyerPublicProfilePage', function (req, res) {
	console.log('in the edit profile page wiht the session id ' + req.session.username);
	pool.query('SELECT * from lawyerprofilemaster where username = $1', [req.session.username], (err, lawyer) => {
		//function to edit lawyer profile
		if (err) {res.render('404.hbs');}
		else if (lawyer==null){res.render('404.hbs', {error:err});}
		else {
			res.render('lawyerProfilePage.hbs', {username : req.session.username, firstname : lawyer.firstname, lastname : lawyer.lastname, photo : lawyer.photo,
				linkedinProfile: lawyer.linkedinProfile, 
		    	pricePerHour : lawyer.pricePerHour, graduationDate : lawyer.graduationDate, graduationUniversity : lawyer.graduationUniversity, address : lawyer.address
		    	, professionalExperience : lawyer.professionalExperience, username : lawyer.username });			
		}
	})
	
});

		

router.post('/sendFileForGLOForm', upload.single("filetoupload" /* name attribute of <file> element in your form */), function (req, res) {
	
	console.log('in the sendFileForGLOForm function ');
	console.log('dirname is ' + __dirname);
	console.log('resolved path is ' + path.resolve('./'));
	console.log('filename is ' + req.file.originalname);
	console.log('path is ' + req.file.path);
	console.log('dirname is ' + __dirname);
    const tempPath = req.file.path;
    const targetPath = path.join(path.resolve('./'), "./views/img/image.png");

      fs.rename(tempPath, targetPath, function(err) {
        if (err) console.log('error is ' + err);

        res
          .status(200)
          .contentType("text/plain")
          .end("File uploaded!");
        console.log('file is uploaded!');
      });
//    else {
//      fs.unlink(tempPath, function(err){
//        if (err) console.log('error is ' + err);
//
//        res
//          .status(403)
//          .contentType("text/plain")
//          .end("Only .png files are allowed!");
//      });
//    }
  });


//GET route for reading all questions
router.get('/questionDashboard', function (req, res) {
    //pull out all the recently answered questions on the platform
    pool.query('SELECT * from qamaster ORDER BY responseDate DESC', (err, qamaster) => {
	            if (!qamaster){
	            	console.log('record list not found');
	            } 
	            if(err){
	            	console.log('err ' + err);
	            }
	            else{
	            	console.log('list is found');
	            	var qaList = [];
	            	for(var i = 0; i < qamaster.length; i++){
	            		if (qamaster[i].response == true) {
	            			currentDate = moment().get('date');	            			
	            			if(i==0){
	            				dateGap = currentDate - moment(qamaster[i].responseDate).get('date');
	            				console.log('dateGap is ' + dateGap);
	            			}
	            			recordedDate = moment(qamaster[i].responseDate).get('date');
	            			newDate = moment(qamaster[i].responseDate).set('date', recordedDate + dateGap - 1);	            			
    	            		var qaRecord = {
    	            				'subject':qamaster[i].subject,
    	            				'username' : qamaster[i].username,
    	            				'slugURL':qamaster[i].slugURL,
    	            				'question':qamaster[i].question,    	            				
    	            				'responseDate' : newDate.locale('en').format('lll')
    	            		}
	            		}
	            		qaList.push(qaRecord);
	            	}
	            res.render('dashboard.hbs', {qaList : qaList});
	            }
    }); 

});


router.post('/questionIndexPage', function(req, res){
	req.session.questionasked = req.body.content;
	emailID = req.body.email;
	res.render('login.hbs', {questionAsked:req.session.questionasked});
});

//
//LAWYER SPECIFIC END POINTS
//

//POST route for lawyer login
router.post('/loginLawyer', function (req, res, next) {
  var errors = req.validationErrors();
  if (req.body.email &&
    req.body.password) {
    // If we found a token, find a matching user
	  pool.query('SELECT * from users where email = $1', [req.body.email], (error, user) => {
		console.log('user.rowCount ', user.rowCount);
        if (error || user.rowCount == 0){
        	console.log('error ' + error);
        	console.log('user ' + user);
        	var err = new Error("User not found");
        	res.render('login.hbs', {success: req.session.success, err: err, signup: 'signup'});
        } 
        else if (!user.rows[0].isverified){
        	var err = new Error("The account has not been verified. Please click on the confirmation link sent" + 
    		"to your mailbox to continue");
        	res.render('login.hbs', { success: req.session.success, successMessage: "Your mail has been verified. Please login", signup: 'signup'});
        }
        else {
        	req.session.email = req.body.email;
        	req.session.user = user.rows[0];
        	//find cases where no one has sent a quote yet
        	pool.query ('SELECT * FROM qamaster where status = $1', [Constants.OPEN], (err, qamaster) => {
        	            if (!qamaster){
        	            	console.log('record list not found');
        	            } 
        	            if(err){
        	            	console.log('err ' + err);
        	            }
        	            else{
        	            	console.log('list is found');
        	            	var qaList = [];
        	            	for(var i = 0; i < qamaster.rowCount; i++){
        	            			var slugURL;
	        	            		if(qamaster.rows[i].status == Constants.QUOTE_ACCEPTED){ slugURL = '/case/:' + qamaster.rows[i].slugurl; }
	        	            		else{ slugURL = '/quote/:' + qamaster.rows[i].slugurl; }        	            		
            	            		var qaRecord = {
            	            				'subject' : qamaster.rows[i].subject,
            	            				'createdAt' : moment(qamaster.rows[i].createdat).locale('en').format('lll'),
            	            				'slugURL' : slugURL,
            	            				'startDate' : qamaster.rows[i].startdate,
            	            				'endDate' : qamaster.rows[i].enddate,
            	            				'quoteAmount' : qamaster.rows[i].quoteamount,
            	            				'status' : Constants.QUOTE_NOT_SENT
            	            		}
        	            		qaList.push(qaRecord);
        	            	}
        	            	//add records from lawyerquotemaster corresponding to quotes that lawyer in question has provided
        		            pool.query('SELECT * FROM qamaster INNER JOIN lawyerquotemaster ON qamaster.id =lawyerquotemaster.quoteid WHERE lawyerquotemaster.username = $1', [user.rows[0].username], (err, recordSet) => {
                	            if (!recordSet){
                	            	console.log('record list not found');
                	            } 
                	            if(err){
                	            	console.log('err ' + err);
                	            }
                	            else{
                	            	console.log('list is found in inner join');
                	            	for(var i = 0; i < recordSet.rowCount; i++){
	            	            			var slugURL;
	    	        	            		if(recordSet.rows[i].status == Constants.QUOTE_ACCEPTED){ slugURL = '/case/:' + recordSet.rows[i].slugurl; }
	    	        	            		else{ slugURL = '/quote/:' + recordSet.rows[i].slugurl; }        	            		
                    	            		var qaRecord = {
                    	            				'subject' : recordSet.rows[i].subject,
                    	            				'createdAt' : moment(recordSet.rows[i].createdat).locale('en').format('lll'),
                    	            				'slugURL' : slugURL,
                    	            				'startDate' : recordSet.rows[i].startdate,
                    	            				'endDate' : recordSet.rows[i].enddate,
                    	            				'quoteAmount' : recordSet.rows[i].quoteamount,
                    	            				'status' : recordSet.rows[i].status
                    	            		}
                	            		qaList.push(qaRecord);
                	            	}                	            	
                	            }        		            	
        		            });
        	            	//add records from lawyerquotemaster corresponding to quotes that lawyer in question has NOT provided but came from other lawyers
        		            pool.query('SELECT * FROM qamaster INNER JOIN lawyerquotemaster ON qamaster.id =lawyerquotemaster.quoteid WHERE lawyerquotemaster.username <> $1', [user.rows[0].username], (err, recordSet) => {
                	            if (!recordSet){
                	            	console.log('record list not found');
                	            } 
                	            if(err){
                	            	console.log('err ' + err);
                	            }
                	            else{
                	            	for(var i = 0; i < recordSet.rowCount; i++){
	            	            			var slugURL;
	    	        	            		if(recordSet.rows[i].status == Constants.QUOTE_ACCEPTED){ slugURL = '/case/:' + recordSet.rows[i].slugurl; }
	    	        	            		else{ slugURL = '/quote/:' + recordSet.rows[i].slugurl; }        	            		
                    	            		var qaRecord = {
                    	            				'subject' : recordSet.rows[i].subject,
                    	            				'createdAt' : moment(recordSet.rows[i].createdat).locale('en').format('lll'),
                    	            				'slugURL' : slugURL,
                    	            				'startDate' : recordSet.rows[i].startdate,
                    	            				'endDate' : recordSet.rows[i].enddate,
                    	            				'quoteAmount' : recordSet.rows[i].quoteamount,
                    	            				'status' : recordSet.rows[i].status
                    	            		}
                	            		qaList.push(qaRecord);
                	            	}                	            	
                	            }        		            	
        		            });        		            
        	            	req.session.username = user.rows[0].username;
//        	            	if(user.isLawyer) res.render('lawyerDashboard', {success: req.session.success, errors: req.session.errors,
//        	            		qaList: qaList, test: 'test'});
//        	            	res.render('lawyerDashboard', {qaList: qaList});
        	            	if(user.rows[0].islawyer && !user.rows[0].lastlogin) {
						    	pool.query('UPDATE users set lastlogin = $1 where email = $2', [moment().locale('en').format('lll'), req.body.email], (err, success) => {
					                if (err) { return res.status(500).send({ msg: err.message }); }
		        	            	res.render('lawyerprofile', {success: req.session.success, errors: req.session.errors,
		        	            		qaList: qaList, test: 'test', username: user.rows[0].username})
					            });    	            		
        	            	}

        	            	//if the user is a lawyer and is logging in to provide a quote 
//    	            		else if(user.rows[0].islawyer && user.rows[0].lastlogin && req.session.slugURL) {
//						    	pool.query('UPDATE users set lastlogin = $1 where email = $2', [moment().locale('en').format('lll'), req.body.email], (err, success) => {
//					                if (err) { return res.status(500).send({ msg: err.message }); }
//					            }); 
//						        pool.query('select * from qamaster where slugURL = $1', [req.session.slugURL], (err, qaRecord) => {
//						            if (!qaRecord){
//						            	console.log('record not found');
//						            	res.render('404');
//						            }
//						            else{res.render('lawyerQuote', {question: qaRecord.rows[0].question, date: moment(qaRecord.rows[0].createdat).locale('en').format('L'), subject: qaRecord.rows[0].subject, username: qaRecord.rows[0].username, answer: qaRecord.rows[0].answer, loggedIn: true});}
//						        });
//    	            		}
        	            	
    	            		else {
						    	pool.query('UPDATE users set lastlogin = $1 where email = $2', [moment().locale('en').format('lll'), req.body.email], (err, success) => {
					                if (err) { return res.status(500).send({ msg: err.message }); }
					            });    	            			
    	            			res.render('lawyerDashboard', {qaList: qaList, username : req.session.username})}
        	            } 
        	        });        	
        }
    });    
  }  else {
    var err = new Error('All fields are required');
    let email = req.body.email;
//    req.checkBody('email', 'email is required').notEmpty();    
    err.status = 400;
    req.session.errors = errors;
    req.session.success = false;
    res.render('login.hbs', { success: req.session.success, err: err, errors: req.session.errors, signin: 'signin'
    });
    
//    return next(err);
  }
})

//POST for a lawyer providing a quote to a case
router.post('/responseQuote', function(req, res, next){
	pool.query('SELECT * from qamaster where slugURL = $1', [req.session.slugURL], (err, qaRecord) =>{
		if (err){console.log('err is ', err);res.render('404'), {error:err}}
		else{
		    pool.query('INSERT into lawyerquotemaster(username, quoteID, quoteValue, quoteDescription, createdAt, status) values($1, $2, $3, $4, $5, $6)', [req.session.user['username'], qaRecord.rows[0].id, req.body.quoteDescription, req.body.quoteAmount, new Date(), Constants.QUOTE_SENT], function (err, quoteRecord) {
		        if (!quoteRecord){
		        	console.log('err');
		        	res.render('404');
		        } 
		        if(err){
		        	console.log('err');
		        	res.render('404');
		        }
		        else{
		        	pool.query('UPDATE qamaster SET status = $1 where slugURL = $2', [Constants.QUOTE_RECEIVED, req.session.slugURL], (err) => {
				        if (!quoteRecord){
				        	console.log('err');
				        	res.render('404');
				        } 
				        if(err){
				        	console.log('err');
				        	res.render('404');
				        }	
				        else{
				        	pool.query('SELECT username from qamaster WHERE slugURL = $1', [req.session.slugURL], (err, updatedRecord) => {
						        if (!updatedRecord){
						        	console.log('err');
						        	res.render('404');
						        } 
						        if(err){
						        	console.log('err');
						        	res.render('404');
						        }	
						        else{
						        	pool.query ('SELECT * FROM qamaster where status = $1', [Constants.OPEN], (err, qamaster) => {
				        	            if (!qamaster){
				        	            	console.log('record list not found');
				        	            } 
				        	            if(err){
				        	            	console.log('err ' + err);
				        	            }
				        	            else{
				        	            	var qaList = [];
				        	            	for(var i = 0; i < qamaster.rowCount; i++){
				            	            		var qaRecord = {
				            	            				'subject' : qamaster.rows[i].subject,
				            	            				'createdAt' : moment(qamaster.rows[i].createdat).locale('en').format('lll'),
				            	            				'slugURL' : qamaster.rows[i].slugurl,
				            	            				'startDate' : qamaster.rows[i].startdate,
				            	            				'endDate' : qamaster.rows[i].enddate,
				            	            				'quoteAmount' : qamaster.rows[i].quoteamount,
				            	            				'status' : Constants.QUOTE_NOT_SENT
				            	            		}
				        	            		qaList.push(qaRecord);
				        	            	}
				        	            	//add records from lawyerquotemaster corresponding to quotes that lawyer in question has provided
				        		            pool.query('SELECT * FROM qamaster INNER JOIN lawyerquotemaster ON qamaster.id = lawyerquotemaster.quoteid WHERE lawyerquotemaster.username = $1', [req.session.user['username']], (err, recordSet) => {
				                	            if (!recordSet){
				                	            	console.log('record list not found');
				                	            } 
				                	            if(err){
				                	            	console.log('err ' + err);
				                	            }
				                	            else{
				                	            	for(var i = 0; i < recordSet.rowCount; i++){
				                	            		var slugURL;
				                	            		if(recordSet.rows[i].status == Constants.QUOTE_ACCEPTED){ slugURL = '/case/:' + recordSet.rows[i].slugurl; }
				                	            		else{ slugURL = '/quote/:' + recordSet.rows[i].slugurl; }
			                    	            		var qaRecord = {
			                    	            				'subject' : recordSet.rows[i].subject,
			                    	            				'createdAt' : moment(recordSet.rows[i].createdat).locale('en').format('lll'),
			                    	            				'slugURL' : slugURL,
			                    	            				'startDate' : recordSet.rows[i].startdate,
			                    	            				'endDate' : recordSet.rows[i].enddate,
			                    	            				'quoteAmount' : recordSet.rows[i].quoteamount,
			                    	            				'status' : recordSet.rows[i].status
			                    	            		}
				                	            		qaList.push(qaRecord);
				                	            	}                	            	
				                	            }        		            	
				        		            });
				        	            	//add records from lawyerquotemaster corresponding to quotes that lawyer in question has NOT provided but came from other lawyers
				        		            pool.query('SELECT * FROM qamaster INNER JOIN lawyerquotemaster ON qamaster.id =lawyerquotemaster.quoteid WHERE lawyerquotemaster.username <> $1', [req.session.user['username']], (err, recordSet) => {
				                	            if (!recordSet){
				                	            	console.log('record list not found');
				                	            } 
				                	            if(err){
				                	            	console.log('err ' + err);
				                	            }
				                	            else{
				                	            	for(var i = 0; i < recordSet.rowCount; i++){
				                    	            		var qaRecord = {
				                    	            				'subject' : recordSet.rows[i].subject,
				                    	            				'createdAt' : moment(recordSet.rows[i].createdat).locale('en').format('lll'),
				                    	            				'slugURL' : recordSet.rows[i].slugurl,
				                    	            				'startDate' : recordSet.rows[i].startdate,
				                    	            				'endDate' : recordSet.rows[i].enddate,
				                    	            				'quoteAmount' : recordSet.rows[i].quoteamount,
				                    	            				'status' : Constants.QUOTE_NOT_SENT
				                    	            		}
				                	            		qaList.push(qaRecord);
				                	            	}                	            	
				                	            }        		            	
				        		            });
				        	            	//send email to client that a lawyer has put in a quote
				        	            	pool.query('SELECT * from users where username = $1', [updatedRecord.rows[0].username], (err, client) => {
				        	    		        if(err){
				        	    		        	console.log('err');
				        	    		        	res.render('404', {error : err});
				        	    		        }
				        	    		        else{
//					    	        	            	var mailOptions = { from: 'contact@helplicit.com', to: client.rows[0].email, subject: '[40Q] New quote received', text: 'Hello,\n\n' + 'A lawyer has sent a quote related to your request. Please login to your account to respond.\n Regards, \n Team 40Q ' };		
//					    			    	    	    // Send the email
//					    		    	    	        transporter.sendMail(mailOptions, function (err, info) {
//					    		    	    	        	   if(err)
//					    		    	    	        	     console.log("transport error is " + err);
//					    		    	    	        	   else
//					    		    	    	        	     console.log("transport info is " + info);
//					    		    	    	        	});        
					    			    	    	   res.render('lawyerDashboard', {qaList: qaList, username : req.session.username, message : "Your quote has been successfully sent to the client"})
				        	    		        	}
				        	            	});
							            }
						            }); 						        	
						        }
				        	});
 				        	
				        }
		        	});
		        } 
		    }); 			
		}
  });//close qamaster query
});

//GET route for responding to a question/case with a quote
router.get('/case/:slug', requireLogin, function (req, res) {
	req.session.slugURL = req.params.slug.substring(1, req.params.slug.length);
    // find the case with the matching ID, load the case dashboard in case the request has been accepted by the client
    pool.query('select * from qamaster where slugURL = $1', [req.params.slug.substring(1, req.params.slug.length)], (err, qaRecord) => {
        if (!qaRecord){
        	console.log('record not found');
        	res.render('404');
        } 
        else if(err){
        	console.log('err ' + err);
        	res.render('404');
        }
        else{
        			console.log('qaRecord.rows[0].question ', qaRecord.rows[0].question);
        			var qaRecord = {
        					question : qaRecord.rows[0].question
        			}
        			res.render('caseDashboard', {qaRecord : qaRecord});
        	}
        })        	
        	//req.session.errors = null;
});

router.get('/caseDashboard', function(req, res){
    res.render('caseDashboard');
});


//CLIENT SPECIFIC END POINTS
//show client dashboard on login if the client has logged in before
router.get('/clientDashboard/:message', function(req, res){
	message = req.params.message.substring(1, req.params.message.length);
	var acceptQuote;
	if(message.includes('acceptQuote')){acceptQuote = 'Your message has been sent to the lawyer and they will contact you shortly.';}
	else if (message.includes('questionPosted')){acceptQuote = 'Thanks! Your question has been sent to the expert pool. They will contact you shortly with quotes.';}
	pool.query('SELECT * from qamaster where username = $1 ORDER BY createdat DESC', [req.session.user['username']], (err, qamaster) => {
        if (!qamaster){
        	console.log('record list not found');
        } 
        if(err){
        	console.log('err ' + err);
        }
        else{
        	console.log('list is found');
        	var qaList = [];
        	for(var i = 0; i < qamaster.rowCount; i++){
            		var qaRecord = {
            				'subject' : qamaster.rows[i].subject,
            				'createdAt' : moment(qamaster.rows[i].createdat).locale('en').format('lll'),
            				'slugURL' : qamaster.rows[i].slugurl,
            				'startDate' : qamaster.rows[i].startdate,
            				'endDate' : qamaster.rows[i].enddate,
            				'quoteAmount' : qamaster.rows[i].quoteamount,
            				'status' : qamaster.rows[i].status
            		}
        		qaList.push(qaRecord);
        	}
        res.render('clientDashboard', {qaList : qaList, message : acceptQuote});
        }		
	});
});

//GET route for client to accept a quote by a certain lawyer
router.get('/acceptQuote/:quoteID', requireLogin, function (req, res) {
	quoteID = req.params.quoteID.substring(1, req.params.quoteID.length);
	pool.query('UPDATE qamaster SET status = $1 WHERE id = $2', [Constants.QUOTE_ACCEPTED, quoteID], (err) => {
        if (err){
        	console.log('record not found');
        	res.render('404');
        } 		
	});
    pool.query('update lawyerquotemaster set status = $1 where quoteid = $2', [Constants.QUOTE_ACCEPTED, quoteID], (err) => {
    	if(err){
        	console.log('err ' + err);
        	res.render('404');
        }
        else{
        		console.log('lawyerquotemaster has been updated');
	            pool.query('SELECT email FROM users INNER JOIN lawyerquotemaster ON users.username =lawyerquotemaster.username WHERE quoteid = $1', [quoteID], (err, record) => {
	//              var mailOptions = { from: 'contact@helplicit.com', to: record.rows[0].email, cc: req.session.user['email'], subject: '[40Q] Intro request from client ', text: 'Hello,\n\n' + 'Dear expert, with regards to your quote, the user has requested a 15 minute intro call.\n Please do the needful.\n Regards,\n Team 40Q.'};
	//              // Send the email
	//              transporter.sendMail(mailOptions, function (err, info) {
	//              	   if(err){
	//              	     console.log(err);
	//              	   	 res.render('404.hbs');}
	//              	   else
	//              	     console.log(info);
	//              	});          	
	      	        res.redirect('/clientDashboard/:acceptQuote');	            	
            });
        	}
        })        	
});

//GET route for client to request an intro call with a certain lawyer
router.get('/requestIntroCall/:quoteID', requireLogin, function (req, res) {
    // find the GLO with the matching ID
    pool.query('SELECT email FROM users INNER JOIN lawyerquotemaster ON users.username =lawyerquotemaster.username WHERE quoteid = $1', [req.params.quoteID.substring(1, req.params.quoteID.length)], (err, record) => {
        if (!record){
        	console.log('record not found');
        	res.render('404');
        } 
        else if(err){
        	console.log('err ' + err);
        	res.render('404');
        }
        else{
//	        	pool.query('SELECT * from users where username = $1', [req.params.quoteID.substring(1, req.params.quoteID.length)], (err, lawyerquotemaster) => {
	        		
//	        	});
//	            var mailOptions = { from: 'contact@helplicit.com', to: record.rows[0].email, cc: req.session.user['email'], subject: '[40Q] Intro request from client ', text: 'Hello,\n\n' + 'Dear expert, with regards to your quote, the user has requested a 15 minute intro call.\n Please do the needful.\n Regards,\n Team 40Q.'};
//	            // Send the email
//	            transporter.sendMail(mailOptions, function (err, info) {
//	            	   if(err){
//	            	     console.log(err);
//	            	   	 res.render('404.hbs');}
//	            	   else
//	            	     console.log(info);
//	            	});         	
    	        res.redirect('/askQuestion/:' + req.session.slugURL + '&:introSuccessMessage');	
        	}
        })        	
});

router.get('/newQuery', function(req, res) {
	console.log('req.body.username ', req.body.username);
	pool.query('SELECT * from users where username = $1', [req.session.user['username']], (err, user) => {
        if (!user){
        	console.log('record list not found');
        } 
        if(err){
        	console.log('err ' + err);
        }	
	res.render('newQuestion', {username: user.rows[0].username});
	});
});


//MISC FUNCTIONS
function loadQAList(username){
	pool.query ('SELECT * from qamaster where response = false and username = $1', [username], (err, qamaster) => {
	    if (!qamaster){
	    	console.log('record list not found');
	    	res.render('404', {err : err});
	    } 
	    if(err){
	    	console.log('err ' + err);
	    	res.render('404', {err : err});
	    }
	    else{
	    	console.log('list is found');
	    	var qaList = [];
	    	for(var i = 0; i < qamaster.rowCount; i++){
	    		if (qamaster.rows[i].response == false) {
	        		var qaRecord = {
	        				'subject':qamaster.rows[i].subject,
	        				'createdAt' : moment(qamaster.rows[i].createdat).locale('en').format('lll'),
	        				'slugURL':qamaster.rows[i].slugurl,
	        				'question': qamaster.rows[i].question
	        		}
	    		}
	    		qaList.push(qaRecord);
	    	}
			return qaList;
			}
	    }); 	
}
 
//Create folder for uploading files.
var filesDir = path.join(path.dirname(require.main.filename), "uploads");

if (!fs.existsSync(filesDir)){
fs.mkdirSync(filesDir);
}


module.exports = router;