$(document).ready( function () {
	 var table = $('#userDataTable').DataTable({
			"sAjaxSource": "/search?searchLawyer=${param1}&domainSelect=${param2}",
			"sAjaxDataProp": "",
			"order": [[ 0, "asc" ]],
			"aoColumns": [
			      { "mData": "id"},
		          { "mData": "address" },
				  { "mData": "name" },
				  { "mData": "status" }
			]
	 })
});