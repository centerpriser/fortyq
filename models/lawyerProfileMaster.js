var mongoose = require('mongoose');
var moment = require('moment');

var LawyerProfileMasterSchema = new mongoose.Schema({
	firstname: { type: String, required: true },
	lastname: { type: String, required: true },
	photo: { type: String, required: false },
	linkedinProfile: { type: String, required: true },	
	areasOfPractice: { type: String, required: true },
	languageSpoken: { type: String, required: true },
	pricePerHour: { type: String, required: true },
	graduationDate:{ type: String, required: true },
	graduationUniversity:{ type: String, required: true },
	professionalExperience: { type: String, required: true },
	username: { type: String, required: true },
	responseCount: { type: Number, required: false, 'default' : 0 },
    createdAt: { type: Date, required: true, 'default': moment().locale('en').format('lll')}
});

var LawyerProfileMaster = mongoose.model('LawyerProfileMaster', LawyerProfileMasterSchema);
module.exports = LawyerProfileMaster;

