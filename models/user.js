var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var moment = require('moment');

var UserSchema = new mongoose.Schema({
  email: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  username: {
    type: String,
    unique: true,
    required: true,
    minlength: 3,
    trim: true
  },
  password: {
    type: String,
    required: true,
    minlength: 3,
  },
  passwordResetToken: {
	    type: String
	  },
  isVerified: { 
	  type: Boolean,
	  'default': false
	  },
  isLawyer: { 
	  type: Boolean,
	  'default': false
	  },
  username: {
    type: String,
    unique: true,
    required: false,
    minlength: 3
  },
  lastLogin: { type: Date, required: false }  
});

//authenticate input against database
UserSchema.statics.authenticate = function (username, email, password, callback) {
	User.findOne({email: email})	
    .exec(function (err, user) {
      if (err) {
        return callback(err)
      } else if (!user) {
    	console.log('user not found');    	  
        var err = new Error('User not found.');
        err.status = 401;
        return callback(err);
      }
      bcrypt.compare(password, user.password, function (err, result) {
        if (result === true) {
          return callback(null, user);
        } else {
          return callback();
        }
      })
    });
}

//hashing a password before saving it to the database
UserSchema.pre('save', function (next) {
  var user = this;
  bcrypt.hash(user.password, null, null, function (err, hash) {
    if (err) {
      return next(err);
    }
    user.password = hash;
    next();
  })
});


var User = mongoose.model('User', UserSchema);
module.exports = User;

