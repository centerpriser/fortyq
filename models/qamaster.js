var mongoose = require('mongoose');
var moment = require('moment');

var QAMasterSchema = new mongoose.Schema({
	username: { type: String, required: true },
	subject: { type: String, required: true },
	question: { type: String, required: true },
	answer: { type: String, required: false },
	slugURL: { type: String, required: true },
	response: { type: Boolean, required: false, 'default': false },
	responseBy: { type: String, required: false },
	responseDate: { type: Date, required: false},
	photo: { type: String, required: false },
    createdAt: { type: Date, required: true, 'default': moment().locale('en').format('lll')}
});

var QAMaster = mongoose.model('QAMaster', QAMasterSchema);
module.exports = QAMaster;

