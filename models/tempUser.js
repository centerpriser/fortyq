var mongoose = require('mongoose');
var bcrypt = require('bcrypt');

var TempUserSchema = new mongoose.Schema({
  email: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  username: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  password: {
    type: String,
    required: true,
  },
  GENERATED_VERIFYING_URL: {
	  type: String
  }
});

//authenticate input against database
TempUserSchema.statics.authenticate = function (usename, email, password, callback) {
//  User.findOne({$or: [{email: email}, {username: username}]})
	User.findOne({email: email})	
    .exec(function (err, user) {
      if (err) {
        return callback(err)
      } else if (!user) {
        var err = new Error('User not found.');
        err.status = 401;
        return callback(err);
      }
      bcrypt.compare(password, user.password, function (err, result) {
        if (result === true) {
          return callback(null, user);
        } else {
          return callback();
        }
      })
    });
}

//hashing a password before saving it to the database
TempUserSchema.pre('save', function (next) {
  var user = this;
  bcrypt.hash(user.password, 10, function (err, hash) {
    if (err) {
      return next(err);
    }
    user.password = hash;
    next();
  })
});


var User = mongoose.model('tempUser', TempUserSchema);
module.exports = User;

