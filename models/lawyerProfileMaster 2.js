var mongoose = require('mongoose');
var moment = require('moment');

var LawyerProfileMasterSchema = new mongoose.Schema({
	firstname: { type: String, required: true },
	lastname: { type: String, required: true },
	photo: { type: String, required: false },
	linkedinProfile: { type: String, required: true },	
	areasOfPractice: { type: String, required: true },
	languageSpoken: { type: String, required: true },
	pricePerHour: { type: String, required: true },
	priceOfFirstMeeting: { type: String, required: true },
	selectDay1: { type: String, required: true },
	selectStartTime1: { type: String, required: true },
	selectEndTime1: { type: String, required: true },
	selectDay2: { type: String, required: true },
	selectStartTime2: { type: String, required: true },
	selectEndTime2: { type: String, required: true },
	selectDay3: { type: String, required: true },
	selectStartTime3: { type: String, required: true },
	selectEndTime3: { type: String, required: true },
	selectDay4: { type: String, required: true },
	selectStartTime4: { type: String, required: true },
	selectEndTime4: { type: String, required: true },	
	selectDay5: { type: String, required: true },
	selectStartTime5: { type: String, required: true },
	selectEndTime5: { type: String, required: true },
	graduationDate:{ type: String, required: true },
	graduationUniversity:{ type: String, required: true },
	address: { type: String, required: true },
	region: { type: String, required: true },
	city: { type: String, required: true },
	professionalExperience: { type: String, required: true },
	username: { type: String, required: true },
	responseCount: { type: Number, required: false, 'default' : 0 },
    createdAt: { type: Date, required: true, 'default': moment().locale('en').format('lll')}
});

var LawyerProfileMaster = mongoose.model('LawyerProfileMaster', LawyerProfileMasterSchema);
module.exports = LawyerProfileMaster;

